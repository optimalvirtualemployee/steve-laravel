@component('mail::message')
# Book Session

<strong>{{$user->username}}</strong>! Thanks for registring with us.
<div>
	<p>UserName: {{$user->username}}</p>
	<p>FullName: {{$user->name}}</p>
	<p>Email: {{$user->email}}</p>
</div>

Thanks,<br>
{{ config('app.name') }}
