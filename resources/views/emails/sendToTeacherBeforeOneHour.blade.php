@component('mail::message')


<strong>{{$meeting->teacher->name}}</strong>! You have a session with {{$meeting->username->name}} at {{$meeting->date}} {{$meeting->timeSlot->name}}.
<div>
	<p>program: {{$meeting->program->name}}</p>
	<p>Subject: {{$meeting->subject->name}}</p>
	<p>Topic: {{$meeting->topic->title}}</p>
	<p>Topic: {{$meeting->meetingUrl}}</p>
</div>

Thanks,<br>
{{ config('app.name') }}