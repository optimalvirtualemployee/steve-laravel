@component('mail::message')
# Book Session

<strong>{{$meeting->username->name}}</strong>! Your session have been booked successfully.
<div>
	<p>program: {{$meeting->program->name}}</p>
	<p>Subject: {{$meeting->subject->name}}</p>
	<p>Topic: {{$meeting->topic->title}}</p>
</div>

Thanks,<br>
{{ config('app.name') }}
