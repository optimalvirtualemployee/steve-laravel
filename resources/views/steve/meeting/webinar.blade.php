@extends('layout.steve')
@section('content')
<main>
   <!-- confirm purchase Start-->
   <section class="confirm-purchase-area">
      <div class="container">
         <!-- Section Tittle -->
         <div class="row">
            <div class="col-lg-12">
               <div class="section-tittle text-center mb-4 mt-5">
                  <h2>Webinar Session</h2>
               </div>
            </div>
         </div> 
         <div class="custom-payment-section p-4">
             <div class="row">

                @isset($meeting->meetingUrl)
                <div class="col-lg-12">
                   <div class="inner d-flex justify-content-between align-items-center downloadAttached">
                       <h3>Start your session</h3>
                       <div><a href="{{$meeting->meetingUrl}}" class="btn green-primary-btn" target="_blank"><i class="fa fa-play"></i> Launch Now</a></div>
                   </div>
                </div>
                @endif

                <div class="col-lg-12">
                     <div class="inner">
                         @isset($meeting->description)<p>{{strip_tags($meeting->description)}}</p> @endif
                     </div>
                 </div>

               @isset($meeting->pdf->name)
                <div class="col-lg-12">
                   <div class="inner d-flex justify-content-between align-items-center downloadAttached">
                       <h3>Download The Attached Resources</h3>
                       <div><a href="{{ URL::to('/') }}/uploads/meeting/{{ $meeting->pdf->name }}" class="btn green-primary-btn" download><i class="fa fa-download"></i> Download Now</a></div>
                   </div>
                </div>
                @endif

                 <div class="col-lg-12">
                     <div class="inner">
                         <h2>Practice Question</h2>
                     </div>
                 </div>
                 <div class="col-lg-12">
                     <div class="inner">
                         <div class="questionAll">
                           <?php $i = 1;?>
                           @foreach($question as $value)
                             <div class="questionCn mb-4">
                                 <div class="askedQuestion mb-2"><span class="qCount">{{$i++}}: </span>{{strip_tags($value->question)}}</div>
                                 <div class="answerCn">

                                    @foreach($questionOption as $val)
                                    @if($value->id == $val->questionId)
                                     <div class="">
                                         <label class="option">{{$val->option}}<input type="radio" name="r{{$val->questionId}}" value="{{$val->isCorrect}}" class="radios"><span class="checkmark"></span></label>
                                     </div>
                                    @endif
                                    @endforeach 
                                 </div>
                             </div>
                           @endforeach 
                         </div>
                         <div class="score">
<!--                              <p class="testScore">Your Score is <strong id="rightAnswers">0</strong></p>
 -->                             <button class="btn green-btn-primary" id="submitQuestion">Check Your Answers</button>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12">
                     <div class="inner mt-5">
                         <div id="accordion">
                           @if(count($sessionContant) != NULL)
                           @php($count=0)
                           @foreach($sessionContant as $value)
                           @php($count++)
                              <div class="card">
                                <div class="card-header" id="heading{{$count}}">
                                  <h5 class="mb-0">
                                    <button class="border-0 bg-light text-dark" data-toggle="collapse" data-target="#collapse{{$count}}" aria-expanded="true" aria-controls="collapse{{$count}}">
                                       {{$value->title}}
                                    </button>
                                  </h5>
                                </div>
                            
                                <div id="collapse{{$count}}" class="collapse @if($count == 1) show @endif" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                   {{$value->description}}
                                  </div>
                                </div>
                              </div>
                              @endforeach
                              @endif
                        </div>
                     </div>
                 </div>
             </div>
         </div>
      </div>
   </section>
</main>
<script>
    $(document).ready(function(){
        $('#submitQuestion').on('click',function(){
          var totalAttemptedQuestion = '';
         if($('.questionCn .answerCn input:checked').length != 0){
             
            $('.questionCn .answerCn input:checked').each(function(index, value) {
                if($(this).val()==0){
                 $(this).next('span').addClass('crossmark').removeClass('checkmark');
                }
                 if($(this).val()==1){
                    totalAttemptedQuestion = $('.questionCn .answerCn input:checked').length;
                 }
          });
          
         $('.questionCn .answerCn input').each(function(index, value) {
             if($(this).val() == 1){
                $(this).next('span').addClass('checkmark').removeClass('crossmark');
                //$(this).attr("checked", true); 
                $(this).addClass('correctAnswer');
             }
          });
          
          const wrongQuestion = $('.questionAll .questionCn .option .crossmark').length;
          const rightAnswersGiven = totalAttemptedQuestion  - wrongQuestion ;
          rightAnswersGiven < 0 ? $('#rightAnswers').text('0') : $('#rightAnswers').text(rightAnswersGiven);
        }
        else{
            alert('Attempt one question minimum');
        }
        });
        
        //on Radio button change 
        $('.answerCn .option .radios').click(function() {
            $(this).parents('.answerCn').find('.crossmark').addClass('checkmark').removeClass('crossmark');
            $(this).parents('.answerCn').find('input').removeClass('correctAnswer');
        });
    
    })
    
</script>
@endsection