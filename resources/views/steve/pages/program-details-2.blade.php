@extends('layout.steve')
@section('content')

<style>
    .select-it .active{
        background: rgb(39,80,211);
        background: linear-gradient(
90deg
, rgba(39,80,211,0.6674019949776786) 0%, rgba(61,202,255,1) 100%);
    border: 1px solid #0095ff;
    color: #fff;
    }
    .select-for{
        padding: 7px;
        margin-bottom: 7px;
        border: 1px solid;
        cursor:pointer;
        position:relative;
        width: 200px;
    }
    .select-it .active:after{
      content: "✔ ";
      position: absolute;
      right: 15px;
    }
    .gred-sec{
        background: rgb(39,109,211);
        background: linear-gradient(90deg, rgba(39,109,211,1) 0%, rgba(61,202,255,1) 100%);
    }
    /******************  News Slider Demo-4 *******************/
.post-slide4{
    margin: 0 10px;
    background:#fff;
    box-shadow: 0 1px 2px rgb(43 59 93 / 30%);
    margin-bottom:2em;
    border:1px solid #ededed;
}
.post-slide4 .post-info{
    padding: 5px 10px;
    margin: 0;
    list-style: none;
}
.post-slide4 .post-info li{
    display: inline-block;
    margin: 0 5px;
}
.post-slide4 .post-info li i{
    margin-right: 8px;
}
.post-slide4 .post-info li a{
    font-size: 11px;
    font-weight: bold;
    color: #7e828a;
    text-transform: uppercase;
}
.post-slide4 .post-info li a:hover{
    color: #1dcfd1;
    text-decoration: none;
}
.post-slide4 .post-img{
    position: relative;
}
.post-slide4 .post-img:before{
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    background: rgb(39,80,211);
    background: linear-gradient(90deg, rgba(39,80,211,0.5721638997395833) 0%, rgba(61,202,255,1) 100%);
    transition: opacity 0.40s linear 0s;
}
.post-slide4:hover .post-img:before{
    opacity: 1;
}
.post-slide4 .post-img img{
    width: 100%;
    height: auto;
}
.post-slide4 .read{
    position: absolute;
    bottom: 30px;
    left: 50px;
    font-size: 14px;
    color: #fff;
    text-transform: capitalize;
    opacity: 0;
    transition: all 0.40s linear 0s;
}
.post-slide4:hover .read{
    opacity:1;
}
.post-slide4 .read:hover{
    text-decoration: none;
    color: #1dcfd1;
}
.post-slide4 .post-content{
    padding: 40px 15px;
    position: relative;
}
.post-slide4 .post-author{
    width: 75px;
    height: 75px;
    border-radius: 50%;
    position: absolute;
    top: -45px;
    right: 10px;
    overflow: hidden;
    border:4px solid #fff;
}
.post-slide4 .post-author img{
    width: 100%;
    height: auto;
}
.post-slide4 .post-title{
    font-size: 14px;
    font-weight: bold;
    color: #5d2999;
    margin: 0 0 10px 0;
    text-transform: uppercase;
    transition: all 0.30s linear 0s;
}
.post-slide4 .post-title:after{
    content: "";
    width: 25px;
    display: block;
    margin-top: 10px;
    border-bottom: 4px solid #333;
}
.post-slide4 .post-description{
    font-size: 13px;
    color: #555;
    margin-bottom:20px;
}
.info-wrapper-2 [class^="infobox"] .icon {
    font-weight: normal;
    padding: 15px;
    border: 5px solid;
    border-radius: 50%;
    width: 130px;
    height: 130px;
    display: flex;
    flex-flow: column wrap;
    justify-content: center;
    align-content: center;
    background: #fff;
    overflow: hidden;
}
.info-wrapper-2{
        display: flex;
    flex-flow: wrap;
    justify-content:space-between;
}
.info-wrapper-2 .infobox{
    display: flex;
    justify-content: center;
    align-items: center;
    width:50%;
    margin-bottom: 25px;
}
.info-right .picCir{
    order:2;
    margin-left:10px;
}
.light-gradiant{
background: linear-gradient( 
90deg
 , rgb(229 229 229) 0%, rgb(219 225 255) 100%);
}
#chartdiv0,#chartdiv1,#chartdiv2,#chartdiv3 {
        width: 100%;
        height: 270px;
}
</style>

<main>
   <!-- slider Area Start-->
    <div class="slider-area ">
        <!-- Mobile Menu -->

        <?php 

        if(isset($programs->image->name)){

            $bgURL = URL::to('/').'/uploads/program/'. $programs->image->name ;
        }else{

            $bgURL = '';  
        }
        
        
        
        ?>

        
        <div class="single-slider slider-height2 d-flex align-items-center" style="background-image:
    linear-gradient(to bottom, rgb(112 63 169 / 60%), rgb(0 0 0)),
    url('{{ $bgURL }}')">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            @isset($programs->name)
                            <h2 class="text-white">{{$programs->name}}
                            </h2>@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider Area End-->
   <div class="main-content">
       <section class="p-5">
           <div class="row m-0">
               <div class="col-md-5">
                   <div class="inner">
                        @isset($programs->image->name)
                        <img class="w-100" src=" {{ URL::to('/') }}/uploads/program/{{ $programs->image->name }}" alt="" width="100%"> 
                        @endif
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="inner section-tittle dt-sc-anytitle">
                        <h2>Preparing Student for the Future</h2>
                        <span></span>
                        <p>
                            @isset($programs->description)
                            {!! strip_tags($programs->description) !!}
                            @endif
                        </p>
                    </div>
                </div>
                </div>
        </section>
        <section class="p-5 bg-light">
                <div class="row">
                    <div class="col-md-12">
                        <div class="inner">
                            <div class="text-center  section-tittle dt-sc-anytitle mb-5">
                                <h2 >Choose your curriculum</h2>
                                <span></span>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="inner  d-flex justify-content-center select-it mb-4">
                                        <div class="select-for active w-50">For Me</div>
                                        <div class="select-for w-50">For My Child</div>
                                    </div>
                                    <div class="form-container">
                                         <form class="row contact_form" action="#" method="post" novalidate="novalidate">
                                          <div class="col-md-12 form-group p_star">
                                              <select class="form-control"  id="state">
                                                  <option value="state1">State 1</option>
                                                  <option value="state1">State 2</option>
                                                  <option value="state1">State 3</option>
                                              </select>
                                          </div>
                                          <div class="col-md-12 form-group p_star">
                                            <select class="form-control"  id="year">
                                                  <option value="year">Year 1</option>
                                                  <option value="year">Year 2</option>
                                                  <option value="year">Year 3</option>
                                              </select>
                                          </div>
                                          <div class="col-md-12 form-group p_star">
                                            <select class="form-control"  id="subject">
                                                  <option value="Subject">Subject 1</option>
                                                  <option value="Subject">Subject 2</option>
                                                  <option value="Subject">Subject 3</option>
                                              </select>
                                          </div>
                                          <div class="col-md-12 text-center form-group">
                                            <button type="submit" value="submit" class="btn green-primary-btn">
                                              Submit
                                            </button>
                                          </div>
                                        </form>   
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="inner">
                                        <img src="/uploads/program/cr.png" class="w-100">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <div class="p-5">
        <div class="container">
            <div class="section-tittle dt-sc-anytitle text-center">
                <h2>Brill Learning</h2>
                <span></span>
            </div>
            <div class="info-wrapper-2">
                <div class="infobox">
                   <div class="mr-2 picCir">
                       <span class="icon">
                         <img src="http://stevelms.optimaldevelopments.com/steve/img/learning-new.png">
                       </span> 
                   </div>
                   <div>
                       <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                   </div>
                </div>
                <div class="infobox info-right">
                   <div class="picCir">
                       <span class="icon">
                         <img src="http://stevelms.optimaldevelopments.com/steve/img/learning-new.png">
                       </span> 
                   </div>
                   <div>
                       <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                   </div>
                </div>
                <div class="infobox">
                   <div class="mr-2 picCir">
                       <span class="icon">
                         <img src="http://stevelms.optimaldevelopments.com/steve/img/learning-new.png">
                       </span> 
                   </div>
                   <div>
                       <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                   </div>
                </div>
                <div class="infobox info-right">
                   <div class="picCir">
                       <span class="icon">
                         <img src="http://stevelms.optimaldevelopments.com/steve/img/learning-new.png">
                       </span> 
                   </div>
                   <div>
                       <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                   </div>
                </div>
                <div class="infobox">
                   <div class="mr-2 picCir">
                       <span class="icon">
                         <img src="http://stevelms.optimaldevelopments.com/steve/img/learning-new.png">
                       </span> 
                   </div>
                   <div>
                       <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                   </div>
                </div>
                <div class="infobox info-right">
                   <div class="picCir">
                       <span class="icon">
                         <img src="http://stevelms.optimaldevelopments.com/steve/img/learning-new.png">
                       </span> 
                   </div>
                   <div>
                       <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                   </div>
                </div>
            </div>
        </div>
    </div>
<!--Track your Progress -->
<section class="category-area py-5 bg-light">
   <div class="container">
      <!-- Section Tittle -->
      <div class="row">
         <div class="col-lg-12">
            <div class="section-tittle text-center mb-4">
               <div class="dt-sc-anytitle">
                  <h2>Track your Progress Online</h2>
                  <span></span>
               </div>
               <p class="mb-0">Tell us your Goal</p>
               <p>And Let's work towards it</p>
            </div>
            <!-- Chart Container -->
            <div class="row">
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>In Class Question</h6>
                        <div id="chartdiv0"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>Student Understanding</h6>
                        <div id="chartdiv1"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>Feedback</h6>
                        <div id="chartdiv2"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>Progress to Goal</h6>
                    <div id="chartdiv3"></div>
                    </div>
                </div>
            </div>
            <!-- Chart Container ends -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner text-center mt-4 border-top pt-3">
                        <p>Tell us your goal and let's begin </p>
                        <a href="#" class="btn btn-success">Enter goal</a>
                    </div>
                </div>
            </div>
         </div>
      </div>
      <div class="row">
         
      </div>
   </div>
</section>
<!--Track your Progress ends-->
        <section class="p-5">
           <div class="row m-0">
            <div class="col-md-12">
                <div class="inner">
                    <div class="section-tittle dt-sc-anytitle text-center mb-5">
                         @isset($programs->name)
                            <h2> Meet our Tutor in {{$programs->name}}</h2>
                        @endif
                        <span></span>
                    </div>
                    <div id="news-slider4" class="owl-carousel">
                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 10, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">1</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-1.jpg" alt="">
                            <a href="#" class="read">View Profile</a>
                        </div>
                        <div class="post-content">
                            <span class="post-author">
                                <a href="#"><img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-5.jpg" alt=""></a>
                            </span>
                            <h3 class="post-title">Teacher Name</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, </p>
                        </div>
                    </div>
     
                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 12, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">3</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-2.jpg" alt="">
                            <a href="#" class="read">View Profile</a>
                        </div>
                        <div class="post-content">
                            <span class="post-author">
                                <a href="#"><img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-6.jpg" alt=""></a>
                            </span>
                            <h3 class="post-title">Teacher Name</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, </p>
                        </div>
                    </div>
                    
                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 12, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">3</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-3.jpg" alt="">
                            <a href="#" class="read">View Profile</a>
                        </div>
                        <div class="post-content">
                            <span class="post-author">
                                <a href="#"><img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-7.jpg" alt=""></a>
                            </span>
                            <h3 class="post-title">Teacher Name</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, </p>
                        </div>
                    </div>
                    
                    <div class="post-slide4">
                        <ul class="post-info">
                            <li><i class="fa fa-tag"></i><a href="#">java</a>,<a href="#">php</a></li>
                            <li><i class="fa fa-calendar"></i><a href="#">June 12, 2016</a></li>
                            <li><i class="fa fa-comment"></i><a href="#">3</a></li>
                        </ul>
                        <div class="post-img">
                            <img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-4.jpg" alt="">
                            <a href="#" class="read">View Profile</a>
                        </div>
                        <div class="post-content">
                            <span class="post-author">
                                <a href="#"><img src="http://bestjquery.com/tutorial/news-slider/demo31/images/img-7.jpg" alt=""></a>
                            </span>
                            <h3 class="post-title">Teacher Name</h3>
                            <p class="post-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore ducimus est, excepturi nam odit possimus? Accusantium, </p>
                        </div>
                    </div>
                </div>
                </div>
            </div>  
            <div class="col-md-12">
                <div class="inner text-center">
                    <button class="btn green-primary-btn mt-3 mr-2">Book A Session Now >></button>
                </div>
            </div>
               
            </div>
        </section>
      </div>
   </div>
</main>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script>
    am4core.ready(function () {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        var comingData = [{
            "category": "",
            "value": 80,
            "full": 100
        }, {
            "category": "",
            "value": 35,
            "full": 100
        }, {
            "category": "",
            "value": 92,
            "full": 100
        }, {
            "category": "",
            "value": 68,
            "full": 100
        }]
        
        for (i = 0; i < comingData.length; i++) { 
            var divID="chartdiv"+i;  
            chart = am4core.create(divID, am4charts.RadarChart);
            chart.data = [comingData[i]];
            chartSetting(chart);
        }

        // Create chart instance
        function chartSetting(chart) {
            // Make chart not full circle
            chart.startAngle = -90;
            chart.endAngle = 180;
            chart.innerRadius = am4core.percent(85);

            // Set number format
            // chart.numberFormatter.numberFormat = "#.#'%'";
            chart.numberFormatter.numberFormat = "#.#";

            // Create axes
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.grid.template.strokeOpacity = 0;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.fontWeight = 400;
            categoryAxis.renderer.labels.template.adapter.add("fill", function (fill, target) {
                return (target.dataItem.index >= 0) ? chart.colors.getIndex(target.dataItem.index) : fill;
            });
            categoryAxis.renderer.minGridDistance = 10;

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.strokeOpacity = 0;
            valueAxis.min = 0;
            valueAxis.max = 100;
            valueAxis.strictMinMax = true;

            // Create series
            var series1 = chart.series.push(new am4charts.RadarColumnSeries());
            series1.dataFields.valueX = "full";
            series1.dataFields.categoryY = "category";
            series1.clustered = false;
            series1.columns.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
            series1.columns.template.fillOpacity = 0.08;
            series1.columns.template.cornerRadiusTopLeft = 20;
            series1.columns.template.strokeWidth = 0;
            series1.columns.template.radarColumn.cornerRadius = 20;

            var series2 = chart.series.push(new am4charts.RadarColumnSeries());
            series2.dataFields.valueX = "value";
            series2.dataFields.categoryY = "category";
            series2.clustered = false;
            series2.columns.template.strokeWidth = 0;
            series2.columns.template.tooltipText = "{category}: [bold]{value}[/]";
            series2.columns.template.radarColumn.cornerRadius = 20;

            series2.columns.template.adapter.add("fill", function (fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            // Add cursor
            chart.cursor = new am4charts.RadarCursor();

        }

    }); // end am4core.ready()
</script>
<script type="text/javascript">

$(document).ready(function(){
    
$("#news-slider4").owlCarousel({
        items:4,
        itemsDesktop:[1199,3],
        itemsDesktopSmall:[1000,2],
        itemsMobile:[600,1],
        pagination:false,
        navigationText:false,
        autoPlay:true
    });    
    
$('.select-for').on('click',function(){
    $('.select-for').removeClass('active');
    $(this).addClass('active');
    
})
    
});
 
</script>

@endsection