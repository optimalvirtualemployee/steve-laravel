@extends('layout.steve')

@section('content')

<div class="container p-5">
    

<form onsubmit="return false;" class="w-100 text-center">
      <div id="securepay-ui-container"></div>
      <button class="btn btn-success mr-3" onclick="mySecurePayUI.tokenise();">Submit</button>
      <button class="btn btn-warning"onclick="mySecurePayUI.reset();">Reset</button>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script id="securepay-ui-js" src="https://payments-stest.npe.auspost.zone/v3/ui/client/securepay-ui.min.js"></script>
    <script type="text/javascript">
      var mySecurePayUI = new securePayUI.init({
        containerId: 'securepay-ui-container',
        scriptId: 'securepay-ui-js',
        clientId: '0oaxb9i8P9vQdXTsn3l5',
        merchantCode: '5AR0055',
        card: {
            allowedCardTypes: ['visa', 'mastercard'],
            showCardIcons: true,
            onCardTypeChange: function(cardType) {
              // card type has changed
            },
            onBINChange: function(cardBIN) {
              // card BIN has changed
            },
            onFormValidityChange: function(valid) {
              // form validity has changed
            },
            onTokeniseSuccess: function(tokenisedCard) {
                
                var $cardToken = tokenisedCard.token;
                
               var $code = "<?php echo $code; ?>";
               
              // alert($code);
                
                
            $.ajax({
            type: "POST",
            data: {
            token: $cardToken,
            orderId: <?php echo $orderId; ?>,
            code: $code,
            },
            url: '/frontend/paymentreq',
            dataType: 'json',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
            $(".loading").show();
            },
            complete: function () {
            $(".loading").hide();
            },
            success: function (result) {
                
                console.log(result);
                window.location.replace("../payment-successfull");
        
            }
            });
                
            
            },
            onTokeniseError: function(errors) {
                console.log(errors);
              // tokenization failed
            }
        },
        style: {
          backgroundColor: 'rgba(135, 206, 250, 0.1)',
          label: {
            font: {
                family: 'Arial, Helvetica, sans-serif',
                size: '1.1rem',
                color: 'darkblue'
            }
          },
          input: {
           font: {
               family: 'Arial, Helvetica, sans-serif',
               size: '1.1rem',
               color: 'darkblue'
           }
         }  
        },
        onLoadComplete: function () {
          // the UI Component has successfully loaded and is ready to be interacted with
        }
      });
    </script>
    <?php /*
    <main>
        <!-- confirm purchase Start-->
        <section class="confirm-purchase-area">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4 mt-5">
                            <h2>Payment Option</h2>
                        </div>
                    </div>
                </div>
                <div class="custom-payment-section p-4">
                    <h5>Choose Payment Method</h5>
                    <div class="payment-navtabs">
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <div class="form-item">
                                <label for="radio-1">
                                    <input type="radio" name="radios" id="radio-1"> <span>Credit / Debit Card</span>
                                </label>
                            </div>
                          </li>
                          <li class="nav-item">
                            <div class="form-item">
                                <label for="radio-2">
                                    <input type="radio" name="radios" id="radio-2"><span>Paypal</span>
                                </label>
                            </div>
                          </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="product-card">
                                <div class="product-card-header">
                                    <img src="{{ asset('steve/img/mastercard.png')}}" alt="" class="img-fluid">
                                    <span>3 Credits</span>
                                </div>
                                <div class="product-card-body">
                                    <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h6>
                                </div>
                                <div class="product-card-footer text-right">
                                    <span>$30.00</span>
                                </div>
                            </div>
                            <div class="payment-cards">
                                <img src="{{ asset('steve/img/mastercard.png')}}" alt="" class="img-fluid">
                                <img src="{{ asset('steve/img/visa.png')}}" alt="" class="img-fluid">
                                <img src="{{ asset('steve/img/amex.png')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <div class="card p-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="card-name">Name on Card</label>
                                            <input type="text" class="form-control" id="card-name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="postal-code">Postal Code</label>
                                            <input type="text" class="form-control" id="postal-code">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="card-number">Card Number (no space or dashes)</label>
                                            <input type="number" class="form-control" id="card-number">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="expiry-date">Expiration (mm/yy)</label>
                                            <input type="text" class="form-control" id="expiry-date">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="security-code">CVV</label>
                                            <input type="text" class="form-control" id="security-code">
                                        </div>
                                    </div>
                                </div>
                                <div class="button-box">
                                    <a href="#" class="btn header-btn">Pay Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <?php */ ?>
</div>
    @endsection
