@extends('layout.steve')
@section('content')
@if($programs)
<main>
   <section class="confirm-purchase-area">
      <div class="container-fluid">
         <div class="accordion-wrapper">
            <div class="container-fluid">
               <div class="vertical-tabs-wrapper">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="fade shadow rounded bg-white show active p-5">
                           <div class="row">
                              <div class="col-md-8">
                                 <div class="border-bottom">
                                    @isset($programs->name)
                                    <h1 class="text-dark" style="font-weight: 600;">
                                       {{$programs->name}}
                                    </h1>
                                    @endif
                                    @isset($programs->image->name)
                                    <img class="w-100 my-4" src=" {{ URL::to('/') }}/uploads/program/{{ $programs->image->name }}" alt="" width="100%"> 
                                    @endif
                                    @isset($programs->description)
                                    {!! strip_tags($programs->description) !!}
                                    @endif
                                 </div>
                              </div>
                              <aside class="col-md-4">
                                 <div class="card mb-3">
                                    @if(session()->has('message'))
                                    <div class="alert alert-success" id="successMessage">
                                        {{ session()->get('message') }}
                                    </div>
                                    @endif

                                    @if (!$errors->isEmpty())
                                    <div class="alert alert-danger" id="successMessage">
                                        @foreach($errors->all() as $error)

                                        <p>{{$error}}</p>
                                        @endforeach
                                    </div>
                                    @endif

                                    <h3 class="card-header mb-3">Join our Newsletter</h3>
                                    <form class="form-horizontal" id="newsletterfrm" method="POST" novalidate="" action="{{url('/newsLetter')}}"
                                        enctype='multipart/form-data'>
                                          {{ csrf_field() }}
                                       <div class="form-group">
                                          <div class="col-sm-12">
                                          <p id="err" style="color: red"></p>
                                             <input type="email" class="form-control" id="inputEmail3" name="email" placeholder="Email">
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <div class="col-sm-12">
                                             <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Join Now</button>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                                 <h3>More Program</h3>
                                 <div class="list-group  mb-3">
                                    <?php $i=0;?>
                                    @isset($allPrograms)
                                    @foreach($allPrograms as $value)
                                    
                                    <?php $i++;?>
                                    <a href="{{ $value->slug }} " class="list-group-item @if($i == 1) active @endif">
                                       <h4 class="list-group-item-heading">{{$value->name}}</h4>
                                       <p class="list-group-item-text">
                                          {!! str_limit($value->description,100) !!}
                                       </p>
                                    </a>
                                 
                                    @endforeach
                                    @endif
                                 </div>
                              </aside>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- basic form end -->
               </div>
            </div>
         </div>
      </div>
      </div>
   </section>
</main>

<script type="text/javascript">

$(document).ready(function(){

    $("#newsletterfrm").submit(function(){

        if($("#inputEmail3").val()=="")
        {
            $("#err").text("Please enter email");
            $("#inputEmail3").focus();
            return false;
        }
        });
    });
 
</script>

@else

<?php header("Location: /errors.404");
die(); ?>

@endif

@endsection