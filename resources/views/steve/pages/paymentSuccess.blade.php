@extends('layout.steve')
@section('content')

<main>
   <section class="confirm-purchase-area">
      <div class="container-fluid">
         <div class="accordion-wrapper">
            <div class="container-fluid">
               <div class="vertical-tabs-wrapper">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="fade shadow rounded bg-white show active p-5">
                           <div class="row">
                              <div class="col-12 mt-6">
                                
                                 <h4 class="mb-4" style="text-align: center;">Payment Successfull</h4>

                              </div>      
                             </div>
                           </div>
                        </div>
                     </div>
                     <!-- basic form end -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection