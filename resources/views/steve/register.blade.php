@extends('layout.steve')

@section('content')

<main>
  <section class="confirm-purchase-area">
  <div class="container">
    <div class="d-flex justify-content-md-center align-items-center h-100 my-login-page">
      <div class="card-wrapper">
        <div class="card fat">
          <div class="card-body pt-1">
            @if(isset($editStatus))
            @method('PUT')
            @endif


            @if(session()->has('message'))
            <div class="alert alert-success">
            {{ session()->get('message') }}
            </div>
            @endif

            @foreach($errors->all() as $error)
            <div class="alert alert-danger">
            <li>{{$error}}</li>
            </div>
            @endforeach
            <p id="err" style="color:red;text-align: center;"></p>
            <h4 class="card-title">Registration</h4>
             <form method="POST" id="registerfrm" class="my-login-validation" novalidate="" action="/account/register" enctype='multipart/form-data'>
              {{ csrf_field() }}

              <div class="form-group">
                <label for="email">Username</label>
                <input type="type" id="username" class="form-control" name="username" value="" required autofocus>
                <div class="invalid-feedback">
                  Username
                </div>
              </div>

              <div class="form-group">
                <label for="email">Full name</label>
                <input type="type" id="fullname" class="form-control" name="fullname" value="" required autofocus>
                <div class="invalid-feedback">
                  Full Name
                </div>
              </div>

                <div class="form-group">
                  <label for="email">E-Mail Address</label>
                  <input type="email" id="email" class="form-control" name="email" value="" required autofocus>
                  <div class="invalid-feedback">
                    Email is invalid
                  </div>
                </div>

                <div class="form-group">
                      <label for="password">Password </label>
                      <input name="password" id="password" class="form-control" type="password" onkeyup='check();'>
                  <div class="invalid-feedback">
                    Password is required
                  </div>
              </div>

                <div class="form-group">
                <label for="password">Confirm Password </label>
               <input type="password" name="confirm_password" class="form-control" id="confirmpassword"  onkeyup='check();' /> 
                 <span id='message'></span>
                  <!-- <div class="invalid-feedback">
                  </div> -->
              </div>
                <div class="form-group">
                <label for="email">Role</label>
                <select name="roleId" id="roleId" class="form-control">
                  <option value="">Select Role</option>
                  <option value="3">Parent</option>
                  <option value="4">Student</option>
                </select>
                </div>
                
              <div class="form-group m-0">
                <button type="submit" onclick="registerbtn();" class="btn header-btn btn-block">Register</button>
              </div>
              <div class="mt-4 text-center">
                Already have an account ? <a href="{{ url('/login/') }}">Login Here</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


</main>
<script type="text/javascript">
var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('confirmpassword').value) {
    document.getElementById('message').style.color = 'green';
    document.getElementById('confirmpassword').style.border = '1px solid green';
    document.getElementById('message').innerHTML = 'Password Matched';
  } else {
    document.getElementById('message').style.color = 'red';
    document.getElementById('confirmpassword').style.border = '1px solid red';
    document.getElementById('message').innerHTML = 'Password not matching';
  }
}
</script>
<script type="text/javascript">
$(document).ready(function(){
    $("#registerfrm").submit(function(){
        if($("#username").val()=="")
        {
            $("#err").text("Please enter username");
            $("#username").focus();
            return false;
        }
        if($("#fullname").val()=="")
        {
            $("#err").text("Please enter full name");
            $("#fullname").focus();
            return false;
        }
        if($("#email").val()=="")
        {
            $("#err").text("Please enter email");
            $("#email").focus();
            return false;
        }
        if($("#password").val()=="")
        {
            $("#err").text("Please enter password");
            $("#password").focus();
            return false;
        }
        if($("#confirmpassword").val()=="")
        {
            $("#err").text("Please enter confirm password");
            $("#confirmpassword").focus();
            return false;
        }
        if($("#roleId").val()=="")
        {
            $("#err").text("Please enter user role");
            $("#roleId").focus();
            return false;
        }
        });
    });
</script>
@endsection