@extends('layout.steve')

@section('content')
<style type="text/css">
 .error {
      color: red;
   }
</style>
    <main>
        <!-- confirm purchase Start-->
        <section class="confirm-purchase-area">
            <div class="container">
              <form id="bookSessioninfo" method="post" action="{{route('bookSession.store')}}" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        @if(isset($editStatus))
                        @method('PUT')
                        @endif
              <div class="book-session-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4 mt-5">
                            <h2>Book A Session</h2>
                        </div>
                    </div>
                </div>
                <div class="custom-payment-section p-4">
                      @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                        @endif

                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                  <div class="custom-child-detail">
                    <div class="row align-items-center justify-content-between">
                    @if(auth()->user()->roleId != 4) 
                      <div class="col-md-6">
                          <div class="child-detail-background">
                            <h6>User Type</h6>
                          </div>
                      </div>
                    @endif  

                      @if(auth()->user()->roleId != 4)
                        <div class="col-md-6">
                        <div class="child-detail-background">
                          <div class="form-group m-0">
                              <label class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="enter_you" class="custom-control-input" name="availability" value="option1" checked="checked">
                              <span class="custom-control-label"> You</span>
                              </label>

                              <label class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="childrenSelect" class="custom-control-input" name="availability" value="option1">
                              <span class="custom-control-label"> Your Child</span>
                              </label>
                          </div>
                        </div>
                      </div>
                       @endif 

                    </div>
                  </div>
                  <div class="custom-child-detail mt-5 mb-3">
                    <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="child-detail-background">
                          <h6>Personal Details</h6>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="child-detail-background">
                          <div class="form-group" id="div_childrenSelect">
                              <label for="userId">Select Student</label>

                              <select class="form-control selectpicker" id="userId" name="userId" data-live-search="true">
                                  <option value="">Select Student</option>
                                  @if(isset($user))
                                  @foreach($user as $value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                  @endif
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="firstName">First Name</label>
                              <input id="firstName" class="form-control" type="text" name="firstName" placeholder="First Name">
                          </div>
                          <div class="form-group">
                              <label for="lastName">Last Name</label>
                              <input id="lastName" class="form-control" type="text" name="lastName" placeholder="Last Name">
                          </div>
                          <div class="form-group">
                              <label for="email">Email</label>
                              <input id="email" class="form-control" type="email" name="email" placeholder="Enter Email">
                          </div>
                          <div class="form-group">
                              <label for="contact">Contact Number</label>
                              <input id="contact" class="form-control" type="number" name="contact" placeholder="Enter Contact Number">
                          </div>
                        </div>
                      </div>



                      <div class="col-md-6">
                        <div class="child-detail-background">
                          <h6>Billing Address</h6>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="child-detail-background">
                      
                          <div class="form-group">
                              <label for="state">State</label>
                              <input id="state" class="form-control" type="text" name="state" placeholder="State">
                          </div>
                          <div class="form-group">
                              <label for="postCode">Post Code</label>
                              <input id="postCode" class="form-control" type="text" name="postCode" placeholder="Post Code">
                          </div>
                          <div class="form-group">
                              <label for="country">Country</label>
                              <input id="country" class="form-control" type="text" name="country" placeholder="Enter Country">
                          </div>
                         
                        </div>
                      </div>







                         <div class="offset-md-6 col-md-6">
                        <div class="custom-acception mt-3">
                          <div class="form-group">
                            <label class="custom-control custom-checkbox d-flex flex-column-reverse">
                                <input type="checkbox" name="accept" class="custom-control-input">
                                <span class="custom-control-label"> Accept Terms & Conditions</span>
                            </label>
                          </div>
                          <div class="form-group">
                          <p><a class="btn green-primary-btn next">next</a></p>
                                           
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="class-detail-box" style="display:none;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4 mt-5">
                            <h2>Fill Class Detail</h2>
                        </div>
                    </div>
                </div>
                <div class="custom-payment-section p-4">
                  <div class="custom-child-detail mt-5 mb-3">
                    <div class="child-detail-background">
                       <p id="err" style="color:red;"></p>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Enter State</label>
                            <select class="form-control" id="stateId" name="stateId">
                              <option value="">Select State</option>
                                @if(isset($state))
                                @foreach($state as $value)
                                <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach
                                @endif
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Enter Program</label>
                             <select class="form-control" id="programId" name="programId" data-live-search="true">
                               <option value="">Select Program</option>
                                  @if(isset($program))
                                  @foreach($program as $value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                  @endif
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Enter Subject</label>
                              <select class="form-control" id="subjectId" name="subjectId" disabled>
                                  <option value="">Select Subject</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Enter Topic</label>
                              <select class="form-control" id="topicId" name="topicId" disabled>
                                  <option value="">Select Topic</option>
                              </select>
                          </div>
                        </div>

                        


                        <?php /* ?>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Time Slot</label>
                            <select class="form-control" id="timeSlot" name="timeSlotId" data-live-search="true">
                               <option value="">Select Time Slot</option>
                                  @if(isset($timeSlot))
                                  @foreach($timeSlot as $value)
                                  <option value="{{$value->id}}">{{$value->name}}</option>
                                  @endforeach
                                  @endif
                            </select>
                          </div>
                        </div>
                        <?php */ ?>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>No of Sessions</label>
                            <select class="form-control" name="noOfSessionn" id="noOfSessionn" disabled>
                            <option value="">Please Select</option>
                              <!-- <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option> -->
                            </select>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Amount</label>
                            <input class="form-control" id="amount" name="amount" readOnly>
                             
                          </div>
                        </div>


                      </div>
                    </div>
                    <div class="custom-acception mt-3 text-right">
                      <div class="form-group">
                      <a class="btn btn-primary mt-4 pr-4 pl-4" id="previous" >Previous</a>
                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                  </form>

              <div class="payment-box" style="display:none;">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-4 mt-5">
                            <h2>Payment</h2>
                        </div>
                    </div>
                </div>
                <div class="custom-payment-section p-4">
                    <h5>Choose Payment Method</h5>
                    <div class="payment-navtabs">
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item">
                            <div class="form-item">
                                <label for="radio-1">
                                    <input type="radio" name="radios" id="radio-1"> <span>Credit / Debit Card</span>
                                </label>
                            </div>
                          </li>
                          <li class="nav-item">
                            <div class="form-item">
                                <label for="radio-2">
                                    <input type="radio" name="radios" id="radio-2"><span>Paypal</span>
                                </label>
                            </div>
                          </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="product-card">
                                <div class="product-card-header">
                                    <img src="assets/img/mastercard.png" alt="" class="img-fluid">
                                    <span>3 Credits</span>
                                </div>
                                <div class="product-card-body">
                                    <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h6>
                                </div>
                                <div class="product-card-footer text-right">
                                    <span>$30.00</span>
                                </div>
                            </div>
                            <div class="payment-cards">
                                <img src="assets/img/mastercard.png" alt="" class="img-fluid">
                                <img src="assets/img/visa.png" alt="" class="img-fluid">
                                <img src="assets/img/amex.png" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-6">
                            <div class="card p-3">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="card-name">Name on Card</label>
                                            <input type="text" class="form-control" id="card-name">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="postal-code">Postal Code</label>
                                            <input type="text" class="form-control" id="postal-code">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="card-number">Card Number (no space or dashes)</label>
                                            <input type="number" class="form-control" id="card-number">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="expiry-date">Expiration (mm/yy)</label>
                                            <input type="text" class="form-control" id="expiry-date">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="security-code">CVV</label>
                                            <input type="text" class="form-control" id="security-code">
                                        </div>
                                    </div>
                                </div>
                                <div class="button-box">
                                    <a href="#" class="btn header-btn">Pay Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
        </section>
    </main>
<script src="{{ asset('assets/frontened/js/bookSession.js') }}"></script>

<script type="text/javascript">


      $(document).ready(function(){

      $(".next").click(function(){
        var form = $("#bookSessioninfo");
        form.validate({
          errorElement: 'span',
          errorClass: 'error',
          highlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').addClass("has-error");
          },
          unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.form-group').removeClass("has-error");
          },
          rules: {
            firstName: {
              required: true,
            },
            lastName : {
              required: true,
            },
            email : {
              required: true,
            },
            contact:{
              required: true,
            },
            state:{
              required: true,
            },
            postCode:{
              required: true,
            },
            country:{
              required: true,
            },
            accept:{
              required: true,
            },

          },
          messages: {
            firstName: {
              required: "First name is required",
            },
            lastName : {
              required: "Last name is required",
            },
            email : {
              required: "Email is required",
            },
            contact: {
              required: "Contact is required",
            },
            state: {
              required: "State is required",
            },
            postCode: {
              required: "Post Code is required",
            },
            country: {
              required: "Country is required",
            },
            accept: {
              required: "Please agree with Terms",
            },
          }
        });
        if (form.valid() === true){
          if ($('.book-session-box').is(":visible")){
            current_fs = $('.book-session-box');
            next_fs = $('.class-detail-box');
          }else if($('.class-detail-box').is(":visible")){
            current_fs = $('.class-detail-box');
            next_fs = $('.personal_information');
          }
          
          next_fs.show(); 
          current_fs.hide();
        }
      });

      $('#previous').click(function(){
        if($('.class-detail-box').is(":visible")){
          current_fs = $('.class-detail-box');
          next_fs = $('.book-session-box');
        }else if ($('.personal_information').is(":visible")){
          current_fs = $('.personal_information');
          next_fs = $('.class-detail-box');
        }
        next_fs.show(); 
        current_fs.hide();
      });      
    });

// show/hide user
  $('#div_childrenSelect').hide();
  $("input[id$='childrenSelect']").click(function() {
  $('#div_childrenSelect').show();
  });

  $("input[id$='enter_you']").click(function() {
  $('#div_childrenSelect').hide();
  });

// form validation
$(document).ready(function(){

    $("#bookSessioninfo").submit(function(){

        if($("#stateId ").val()=="")
        {
            $("#err").text("Please select state");
            $("#state").focus();
            return false;
        }
        if($("#programId").val()=="")
        {
            $("#err").text("Please select program");
            $("#programId").focus();
            return false;
        }
        if($("#subjectId").val()=="")
        {
            $("#err").text("Please select subject");
            $("#subjectId").focus();
            return false;
        }
        if($("#topicId").val()=="")
        {
            $("#err").text("Please select topic");
            $("#topicId").focus();
            return false;
        }
        if($("#noOfSessionn").val()=="")
        {
            $("#err").text("Please enter no of session");
            $("#noOfSessionn").focus();
            return false;
        }

        });
    });
 
</script>
@endsection