@extends('layout.steve')

@section('content')

<main>
    @isset($banner)
    <style>
    #chartdiv0,#chartdiv1,#chartdiv2,#chartdiv3 {
        width: 100%;
        height: 270px;
    }
</style>
    <!-- slider Area Start -->
    <div class="slider-area ">
        <!-- Mobile Menu -->
        <div class="slider-active">
            <div class="single-slider slider-height">
                <div class="container">
                    <div class="row d-flex align-items-center justify-content-between">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 d-none d-md-block">
                            <div class="hero__img" data-animation="bounceIn" data-delay=".4s">
                                @if(isset($banner->image->name))
                                <img src=" {{ URL::to('/') }}/uploads/banner/{{ $banner->image->name }}" alt="">
                                @endif
                            </div>
                        </div>
                        @if(isset($banner))
                        <div class="col-xl-5 col-lg-5 col-md-5 col-sm-8">
                            <div class="hero__caption">
                                <span data-animation="fadeInRight" data-delay=".4s">{{$banner->bannerHeading}} </span>
                                <h1 data-animation="fadeInRight" data-delay=".6s">{{$banner->bannerText}} </h1>
                                <p data-animation="fadeInRight" data-delay=".8s">{{$banner->bannerSloganText}} </p>
                                <!-- Hero-btn -->
                                <div class="hero__btn" data-animation="fadeInRight" data-delay="1s">
                                    @if(Auth::guard('front')->user())
                                    <a href="{{$banner->bannerButtonLink}}" class="btn hero-btn">{{$banner->bannerButtonText}} >></a>
                                    @else
                                    <a href="{{ url('/login/') }}" class="btn hero-btn">{{$banner->bannerButtonText}} >></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider Area End-->
    @endif

    <!-- Category Area Start-->
    <section class="category-area py-5">
        <div class="container">
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle text-center mb-4">
                        <div class="dt-sc-anytitle">
                            <h2>Personlised & 1-to-1 Classes with Online Tutors</h2>
                            <span></span>
                        </div>
                        <p>Select the Student years level</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @if($program)
                @foreach($program as $value)
                <div class="col-xl-4 col-lg-6">
                    <div class="single-category mb-30">
                        <a href="{{$value->slug}}">
                            <div class="category-img">


                                @isset($value->image->name)
                                <a href="{{route('programDetail', $value->slug)}}"> <img
                                        src=" {{ URL::to('/') }}/uploads/program/{{ $value->image->name }}" alt=""> </a>
                                @endif
                            </div>
                            <div class="category-caption">
                                <!-- <div class="classes-icon">
                                        <img src="{{ asset('steve/img/flower.png')}}" alt="starr images">
                                        <span>$60</span>
                                    </div> -->
                                <a href="{{route('programDetail', $value->slug)}}">
                                    <h2>{{$value->name}}</h2>
                                </a>
                                <p>{!! str_limit($value->description,110) !!}</p>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- Category Area End-->



    <!-- Shop Method Start-->

    <!-- Brilliant Learning Method Start-->
    <div class="brilinat-learning pb-5 pt-5 d-none">
        <div class="container">
            <div class="section-tittle dt-sc-anytitle text-center">
                <h2>Brill Learning</h2>
                <span></span>
            </div>
            <div class="brill-learning-content pt-3 mt-5">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/learning-new.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#"> Effective Learning</a>
                                </h4>
                                <p class="">Effective Learning from the comfort of your home.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/schedule.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#"> Flexible to your schedule</a>
                                </h4>
                                <p class="">Book a session between 7am to 10pm. 7days a week.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/session.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#">Revisit your session</a>
                                </h4>
                                <p class="">With your permission,the session is recorded and accessible to you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/learning-goals.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#"> Personalised to your needs</a>
                                </h4>
                                <p class="">Personalised to your needs and learning goals.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/contract.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#"> No lock fee No sign up fees</a>
                                </h4>
                                <p class="">No lock in contracts and No sign up fees.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/practice.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#"> Real-time Practice</a>
                                </h4>
                                <p class="">Real-time Practice Questions and Exam papers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="card ">
                            <a class="img-card" href="#">
                                <img src="{{ asset('steve/img/progress.png')}}" />
                            </a>
                            <div class="card-content">
                                <h4 class="card-title">
                                    <a href="#"> Track your progress</a>
                                </h4>
                                <p class="">Track your progressand plan sessions with your tutor. Reach your goals</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="brilinat-learning-box p-5">
        <div class="container">
            <div class="section-tittle dt-sc-anytitle text-center">
                <h2>Brill Learning</h2>
                <span></span>
            </div>
            <ul class="info-wrapper">
                <li class="infobox-1">
                    <a class="row" href="#1">
                        <span class="icon">
                            <img src="{{ asset('steve/img/learning-new.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                Effective Learning
                            </h4>
                            <p>Effective Learning from the comfort of your home.</p>
                        </span>
                    </a>
                </li>
                <li class="infobox-2">
                    <a class="row" href="#2">
                        <span class="icon">
                            <img src="{{ asset('steve/img/schedule.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                Flexible to your schedule
                            </h4>
                            <p>Book a session between 7am to 10pm. 7days a week</p>
                        </span>
                    </a>
                </li>
                <li class="infobox-3">
                    <a class="row" href="#3">
                        <span class="icon">
                            <img src="{{ asset('steve/img/session.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                Revisit your session
                            </h4>
                            <p>With your permission,the session is recorded and accessible to you.</p>
                        </span>
                    </a>
                </li>
                <li class="infobox-4">
                    <a class="row" href="#4">
                        <span class="icon">
                            <img src="{{ asset('steve/img/learning-goals.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                Personalised to your needs
                            </h4>
                            <p>Personalised to your needs and learning goals.</p>
                        </span>
                    </a>
                </li>
                <li class="infobox-5">
                    <a class="row" href="#5">
                        <span class="icon">
                            <img src="{{ asset('steve/img/contract.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                No lock fee No sign up fees
                            </h4>
                            <p>No lock in contracts and No sign up fees.</p>
                        </span>
                    </a>
                </li>
                <li class="infobox-6">
                    <a class="row" href="#6">
                        <span class="icon">
                            <img src="{{ asset('steve/img/practice.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                Real-time Practice
                            </h4>
                            <p>Real-time Practice Questions and Exam papers.</p>
                        </span>
                    </a>
                </li>
                <li class="infobox-7">
                    <a class="row" href="#7">
                        <span class="icon">
                            <img src="{{ asset('steve/img/progress.png')}}" />
                        </span>
                        <span class="info-text">
                            <h4>
                                Track your progress
                            </h4>
                            <p>Track your progressand plan sessions with your tutor. Reach your goals.</p>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="latest-wrapper pt-5 pb-3" style="background: #e7f8bc;">
        <div class="latest-area latest-height d-flex align-items-center">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-xl-5 col-lg-5 col-md-6 ">
                        <div class="latest-caption">
                            <h2>Need a hand? <br>Lets talk</h2>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-6 ">
                        <div class="latest-subscribe-box d-flex">
                            <div class="media contact-info mr-5">
                                <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                                <div class="media-body">
                                    <h3>+1 253 565 2365</h3>
                                    <p>Mon to Fri 9am to 6pm</p>
                                </div>
                            </div>
                            <div class="media contact-info">
                                <span class="contact-info__icon"><i class="ti-email"></i></span>
                                <div class="media-body">
                                    @if(session()->has('message'))
                                    <div class="alert alert-success" id="successMessage">
                                        {{ session()->get('message') }}
                                    </div>
                                    @endif

                                    @if (!$errors->isEmpty())
                                    <div class="alert alert-danger" id="successMessage">
                                        @foreach($errors->all() as $error)

                                        <p>{{$error}}</p>
                                        @endforeach
                                    </div>
                                    @endif
                                    <form method="POST" id="contactfrm" novalidate="" action="{{url('/contact')}}"
                                        enctype='multipart/form-data'>
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" class="form-control" name="email"
                                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                                placeholder="Enter email">
                                            <small id="emailHelp" class="form-text text-muted">We'll never share your
                                                email with anyone else.</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="message">Message</label>
                                            <textarea class="form-control" name="message" placeholder="Message"
                                                id="message" rows="3"></textarea>
                                        </div>
                                        <button type="button" onclick="contactbtn();"
                                            class="btn header-btn">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Track your Progress -->
<section class="category-area py-5">
   <div class="container">
      <!-- Section Tittle -->
      <div class="row">
         <div class="col-lg-12">
            <div class="section-tittle text-center mb-4">
               <div class="dt-sc-anytitle">
                  <h2>Track your Progress Online</h2>
                  <span></span>
               </div>
               <p class="mb-0">Tell us your Goal</p>
               <p>And Let's work towards it</p>
            </div>
            <!-- Chart Container -->
            <div class="row">
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>In Class Question</h6>
                        <div id="chartdiv0"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>Student Understanding</h6>
                        <div id="chartdiv1"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>Feedback</h6>
                        <div id="chartdiv2"></div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <h6>Progress to Goal</h6>
                    <div id="chartdiv3"></div>
                    </div>
                </div>
            </div>
            <!-- Chart Container ends -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="inner text-center mt-4 border-top pt-3">
                        <p>Tell us your goal and let's begin </p>
                        <a href="#" class="btn btn-success">Enter goal</a>
                    </div>
                </div>
            </div>
         </div>
      </div>
      <div class="row">
         
      </div>
   </div>
</section>
     <!--Track your Progress ends-->
        
    </div>
    <!--<div class="tutoring-method-area pt-5">-->
    <!--    <div class="container">-->
    <!--        <div class="row d-flex justify-content-between">-->
    <!--            <div class="col-xl-12 col-12 text-center mb-5">-->
    <!--                <h2 class="">Get your personalised tutoring program, pronto</h2>-->
    <!--                <h5 class="">Enter a few details below</h5>-->
    <!--            </div>-->
    <!--            <div class="col-xl-12 col-12 text-center mb-5">-->
    <!--                <div class="single-element-widget mt-30" data-animation="fadeInRight" data-delay=".4s">-->
    <!--                    <h6 class="mb-30">School years</h6>-->
    <!--                    <div class="default-select" id="default-select">-->
    <!--                        <select>-->
    <!--                            <option value="3">Year 3</option>-->
    <!--                            <option value="4">Year 4</option>-->
    <!--                            <option value="5">Year 5</option>-->
    <!--                            <option value="6">Year 6</option>-->
    <!--                            <option value="7">Year 7</option>-->
    <!--                        </select>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <a href="#" class="btn header-btn mt-4">Next Step >></a>-->
    <!--            </div>-->

    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->



    @if(count($testimonial)>0)
    <div class="testimonial ">
        <div class="container">
            <div class="row">
                <div class="col-xl">
                    <div class="testimonials-section">
                        <?php $i = 1; ?>
                        @foreach($testimonial as $value)
                        <input type="radio" name="slider" title="slide{{ $i }}"
                            <?php if($i==1){ echo 'checked="checked"'; } ?> class="slider__nav" />
                        <?php $i++; ?>
                        @endforeach

                        <div class="slider__inner">

                            @foreach($testimonial as $value)
                            <div class="slider__contents">
                                <quote>&rdquo;</quote>
                                <p class="slider__txt">{!! strip_tags($value->description) !!}</p>
                                <h2 class="slider__caption">{{$value->name}} | {{$value->designation}}</h2>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

</main>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script>
    am4core.ready(function () {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        var comingData = [{
            "category": "",
            "value": 80,
            "full": 100
        }, {
            "category": "",
            "value": 35,
            "full": 100
        }, {
            "category": "",
            "value": 92,
            "full": 100
        }, {
            "category": "",
            "value": 68,
            "full": 100
        }]
        
        for (i = 0; i < comingData.length; i++) { 
            var divID="chartdiv"+i;  
            chart = am4core.create(divID, am4charts.RadarChart);
            chart.data = [comingData[i]];
            chartSetting(chart);
        }

        // Create chart instance
        function chartSetting(chart) {
            // Make chart not full circle
            chart.startAngle = -90;
            chart.endAngle = 180;
            chart.innerRadius = am4core.percent(85);

            // Set number format
            // chart.numberFormatter.numberFormat = "#.#'%'";
            chart.numberFormatter.numberFormat = "#.#";

            // Create axes
            var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "category";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.grid.template.strokeOpacity = 0;
            categoryAxis.renderer.labels.template.horizontalCenter = "right";
            categoryAxis.renderer.labels.template.fontWeight = 400;
            categoryAxis.renderer.labels.template.adapter.add("fill", function (fill, target) {
                return (target.dataItem.index >= 0) ? chart.colors.getIndex(target.dataItem.index) : fill;
            });
            categoryAxis.renderer.minGridDistance = 10;

            var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.strokeOpacity = 0;
            valueAxis.min = 0;
            valueAxis.max = 100;
            valueAxis.strictMinMax = true;

            // Create series
            var series1 = chart.series.push(new am4charts.RadarColumnSeries());
            series1.dataFields.valueX = "full";
            series1.dataFields.categoryY = "category";
            series1.clustered = false;
            series1.columns.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
            series1.columns.template.fillOpacity = 0.08;
            series1.columns.template.cornerRadiusTopLeft = 20;
            series1.columns.template.strokeWidth = 0;
            series1.columns.template.radarColumn.cornerRadius = 20;

            var series2 = chart.series.push(new am4charts.RadarColumnSeries());
            series2.dataFields.valueX = "value";
            series2.dataFields.categoryY = "category";
            series2.clustered = false;
            series2.columns.template.strokeWidth = 0;
            series2.columns.template.tooltipText = "{category}: [bold]{value}[/]";
            series2.columns.template.radarColumn.cornerRadius = 20;

            series2.columns.template.adapter.add("fill", function (fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
            });

            // Add cursor
            chart.cursor = new am4charts.RadarCursor();

        }

    }); // end am4core.ready()
</script>

<script>
$(document).ready(function(){
  var testimonialItems = $('.testimonials-section .slider__inner').children().length * 100 ; 
  console.log(testimonialItems);
  $('.testimonials-section .slider__inner').css('width', testimonialItems +'%' );
});

$(document).ready(function(){
    $("#successMessage").delay(15000).slideUp(300);
})
</script>
@endsection