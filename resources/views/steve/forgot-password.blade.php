@extends('layout.steve')

@section('content')
<main>
	<section class="confirm-purchase-area">
	<div class="container">
		<div class="d-flex justify-content-md-center align-items-center h-100 my-login-page">
			<div class="card-wrapper">
				<div class="card fat">
					<div class="card-body">
						<h4 class="card-title">Forgot Password</h4>
						<form method="POST" class="my-login-validation" novalidate="">
							<div class="form-group">
								<label for="email">E-Mail Address</label>
								<input id="email" type="email" class="form-control" name="email" value="" required autofocus>
								<div class="invalid-feedback">
									Email is invalid
								</div>
							</div>

							<div class="form-group m-0">
								<button type="submit" class="btn header-btn btn-block">Submit</button>
							</div>
							<div class="mt-4">
								Don't have an account? <a href="">Book a session now</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</main>
@endsection