   <footer>

       <!-- Footer Start-->
       <div class="footer-area footer-padding pb-0">
           <div class="container">
               <div class="row d-flex justify-content-between">
                   <div class="col-xl-3 col-lg-3 col-md-5 col-sm-6">
                      <div class="single-footer-caption mb-50">
                        <div class="single-footer-caption mb-30">
                             <!-- logo -->
                            <div class="footer-logo">
                                <a href="/"><img src="{{ asset('steve/img/footer-logo.png')}}" alt=""></a>
                            </div>
                            <div class="footer-tittle">
                                <div class="footer-pera">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore.</p>
                               </div>
                            </div>
                        </div>
                      </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-5">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Quick Links</h4>
                               <ul>
                                <!-- <li><a href="#">Primary 2-6</a></li>
                                <li><a href="#">Secondary 7-10</a></li>
                                <li><a href="#">Senior 11-12</a></li> -->
                                   <li><a href="{{ url('about')}}">About</a></li>
                                   <li><a href="{{ url('offer-discounts')}}"> Offers & Discounts</a></li>
                                   <li><a href="{{ url('get-coupon')}}"> Get Coupon</a></li>
                                   <li><a href="{{ url('contact')}}">  Contact Us</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-7">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>Community</h4>
                               <ul>
                                   <li><a href="{{ url('learners')}}">Learners</a></li>
                                   <li><a href="{{ url('partners')}}">Partners</a></li>
                                   <li><a href="{{ url('teaching-center')}}">Teaching Center</a></li>
                                   <!-- <li><a href="#">Blog</a></li> -->
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-5 col-sm-7">
                       <div class="single-footer-caption mb-50">
                           <div class="footer-tittle">
                               <h4>More</h4>
                               <ul>
                                <li><a href="{{ url('frequently-asked-questions')}}">Frequently Asked Questions</a></li>
                                <li><a href="{{ url('terms-conditions')}}">Terms & Conditions</a></li>
                                <li><a href="{{ url('privacy-policy')}}">Privacy Policy</a></li>
                                <!-- <li><a href="#">Contact</a></li> -->
                                <li><a href="{{ url('report-a-payment-issue')}}">Report a Payment Issue</a></li>
                            </ul>
                           </div>
                       </div>
                   </div>
               </div>
               <!-- Footer bottom -->
               <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-7">
                    <div class="footer-copy-right">
                        <p>
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <a href="#">Steve</a>
  </p>                   </div>
                </div>
                 <div class="col-xl-5 col-lg-5 col-md-5">
                    <div class="footer-copy-right f-right">
                        <!-- social -->
                        <div class="footer-social" style="font-size: 0.5rem;">
                            <a href="#"><i class="fab fa-twitter fa-2x"></i></a>
                            <a href="#"><i class="fab fa-facebook-f fa-2x"></i></a>
                            <a href="#"><i class="fab fa-behance fa-2x"></i></a>
                            <a href="#"><i class="fas fa-globe fa-2x"></i></a>
                        </div>
                    </div>
                </div>
            </div>
           </div>
       </div>
       <!-- Footer End-->

   </footer>
   
	<!-- JS here -->
	
		<!-- All JS Custom Plugins Link Here here -->
        <script src="{{ asset('steve/js/vendor/modernizr-3.5.0.min.js')}}"></script>
		<!-- Jquery, Popper, Bootstrap -->
		<script src="{{ asset('steve/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{ asset('steve/js/vendor/moment.min.js')}}"></script>
        <script src="{{ asset('steve/js/vendor/fullcalendar.min.js')}}"></script>
        <script src="{{ asset('steve/js/popper.min.js')}}"></script>
        <script src="{{ asset('steve/js/bootstrap.min.js')}}"></script>
	    <!-- Jquery Mobile Menu -->
        <script src="{{ asset('steve/js/jquery.slicknav.min.js')}}"></script>

		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="{{ asset('steve/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('steve/js/slick.min.js')}}"></script>

		<!-- One Page, Animated-HeadLin -->
        <script src="{{ asset('steve/js/wow.min.js')}}"></script>
		<script src="{{ asset('steve/js/animated.headline.js')}}"></script>
        <script src="{{ asset('steve/js/jquery.magnific-popup.js')}}"></script>

		<!-- Scrollup, nice-select, sticky -->
        <script src="{{ asset('steve/js/jquery.scrollUp.min.js')}}"></script>
            <!-- <script src="{{ asset('steve/js/jquery.nice-select.min.js')}}"></script> -->
		<script src="{{ asset('steve/js/jquery.sticky.js')}}"></script>
        
        <!-- contact js -->
        <script src="{{ asset('steve/js/contact.js')}}"></script>
        <script src="{{ asset('steve/js/jquery.form.js')}}"></script>
        <script src="{{ asset('steve/js/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('steve/js/mail-script.js')}}"></script>
        <script src="{{ asset('steve/js/jquery.ajaxchimp.min.js')}}"></script>
        
		<!-- Jquery Plugins, main Jquery -->	
        <script src="{{ asset('steve/js/plugins.js')}}"></script>
        <script src="{{ asset('steve/js/main.js')}}"></script>
 <script type="text/javascript">
  //register form validation
  function contactbtn(){
     var isError = false;
     $('#contactfrm').find('input[type=email],textarea').each(function(){
     var palceholder =  $(this).attr('placeholder');
     // alert(palceholder);
     $(this).parent().find('.error').html('');
     $(this).css('border','')
     if($(this).val() == ""){
     $(this).css('border','1px solid red');
      isError = true;'';   
      } 
     });
     if(isError == false){
     $('#contactfrm').submit();
     }
  }
</script>