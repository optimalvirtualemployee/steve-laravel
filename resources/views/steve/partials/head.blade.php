<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Online Courses and Tutor</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.webmanifest">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('steve/img/favicon.ico')}}">
<!-- CSS here -->
<link rel="stylesheet" href="{{ asset('steve/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/flaticon.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/slicknav.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/animate.min.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/fontawesome-all.min.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{ asset('steve/css/slick.css')}}">
<!-- <link rel="stylesheet" href="{{ asset('steve/css/nice-select.css')}}"> -->
<link rel="stylesheet" href="{{ asset('steve/css/style.css')}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
