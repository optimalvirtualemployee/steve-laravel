        <!-- Header Start -->
       <div class="header-area">
            <div class="main-header ">
                <div class="header-top top-bg d-none d-lg-block">
                   <div class="container-fluid">
                       <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left d-flex">
                                     <!-- Logo -->
                            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">
                                <div class="logo">
                                  <a href="/"><img src="{{ asset('steve/img/logo.png')}}" alt=""></a>
                                </div>
                            </div>
                                   
                                </div>
                                <div class="header-info-right">
                                   <ul>
                                      @if(Auth::guard('front')->user())                 
                                      <li><a href="/account/dashboard" class="btn header-btn mr-2">Account >></a></li>
                                      @endif
                                      @if(Auth::guard('front')->user()) 
                                      <li><a href="/book-session" class="btn header-btn">Book a session >></a></li>
                                      @else
                                      <li><a href="/login" class="btn header-btn">Book a session >></a></li>
                                      @endif
                                      @if(Auth::guard('front')->user())
                                      <li class="ml-2"><a href="{{ url('/logout/') }}" class="btn btn-danger header-btn">Logout</a></li>
                                      @else
                                      <li class="ml-2"> <a href="{{ url('/login/') }}" class="btn header-btn ">Sign in >></a></li>
                                      @endif
                                   </ul>
                                </div>
                            </div>
                       </div>
                   </div>
                </div>
               <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">                           
                            <div class="col-md-12 text-center">
                                <!-- Main-menu -->
                                <div class="main-menu d-none d-lg-block">
                                    <nav>                                                
                                        <ul id="navigation">  

                                        @if(isset($program))                     
                                        @foreach($program as $value)
                                        <li><a href="{{route('programDetail', $value->slug)}}">{{$value->name}}</a></li>
                                        @endforeach
                                        @endif
                                        </li>
                                        <li class="float-right">
                                          <a class="call-us" href="tel:+496170961709"> CALL US NOW</a> 
                                        </li>
                                         @if(Auth::guard('front')->user()) 
                                        <li  class="float-right stickyButton"><a href="/book-session" class="btn green-primary-btn mt-3 mr-2">Book a session >></a></li>
                                        @else
                                        <li  class="float-right stickyButton"><a href="/login" class="btn green-primary-btn mt-3 mr-2">Book a session >></a></li>
                                        @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div> 
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
       </div>
        <!-- Header End -->