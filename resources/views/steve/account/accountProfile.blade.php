@extends('layout.steve')

@section('content')

<main>
    <section class="confirm-purchase-area">
        <div class="container-fluid">
            <div class="accordion-wrapper">
                <div class="container-fluid">
                    <div class="vertical-tabs-wrapper">
                        <div class="row">
                            <div class="col-md-3">

                                @include('steve/account.verticalLinks')

                            </div>


                            <div class="col-md-9">

                                <div class="fade shadow rounded bg-white show active p-5">

                                    <div class="row">

                                        <div class="col-12 mt-6">
                                            <h4 class="mb-4">Edit Profile</h4>
                                        </div>

                                        @if(isset($user->image->name))

                                        <div class="col-12 mt-6">
                                            <div class="upload-image">
                                                <img width="100" height="60"
                                                    src=" {{ URL::to('/') }}/uploads/user/{{ $user->image->name }}"
                                                    alt="image">
                                            </div>
                                        </div>
                                        @endif

                                        <div class="col-12 mt-6">
                                            <form id="userForm" method="post"
                                                action="{{ url('account-dashboard') }}/{{$user['id']}}"
                                                enctype='multipart/form-data'>

                                                {{ csrf_field() }}

                                                @if(isset($editStatus))
                                                @method('POST')
                                                @endif


                                                @if(session()->has('message'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('message') }}
                                                </div>
                                                @endif

                                                @if (!$errors->isEmpty())
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() as $error)

                                                    <p>{{$error}}</p>
                                                    @endforeach
                                                </div>
                                                @endif
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="username">Username</label>
                                                            <input type="text" class="form-control" id="username"
                                                                name="username" placeholder="Enter username"
                                                                value="{{old('username',  isset($user->username) ? $user->username : NULL)}}"
                                                                <?php if(isset($editStatus)){echo "readonly";} ?>>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="name">Name</label>
                                                            <input type="text" class="form-control" id="name"
                                                                name="name" placeholder="Enter name"
                                                                value="{{old('name',  isset($user->name) ? $user->name : NULL)}}">
                                                        </div>
                                                    </div>

                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="email">Email</label>
                                                            <input type="text" class="form-control" id="email"
                                                                name="email" placeholder="Enter email"
                                                                value="{{old('email',  isset($user->email) ? $user->email : NULL)}}" <?php if(isset($editStatus)){echo "readonly";} ?>>
                                                        </div>
                                                    </div>


                                                        <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="meetingUsername">Meeting User ID</label>
                                                                <input type="text" class="form-control" id="meetingUsername" name="meetingUsername"
                                                                    placeholder="Enter meeting user Id"
                                                                    value="{{old('meetingUsername',  isset($user->meetingUsername) ? $user->meetingUsername : NULL)}}" <?php if(isset($editStatus)){echo "readonly";} ?>>
                                                            </div>
                                                        </div>
                                                         <div class="col-6">
                                                            <div class="form-group">
                                                                <label for="meetingPassword">meeting User Password</label>
                                                                <input type="text" class="form-control" id="name" name="meetingPassword"
                                                                    placeholder="Enter meeting user password"
                                                                    value="{{old('meetingPassword',  isset($user->meetingPassword) ? $user->meetingPassword : NULL)}}" <?php if(isset($editStatus)){echo "readonly";} ?>>
                                                            </div>
                                                        </div>



                                                    <div class="col-12 mt-6">
                                                        <div class="form-group inputDnD">
                                                            <label for="name">Image</label>
                                                            <input type="file" name="image" class="form-control-file text-muted font-weight-bold">
                                                        </div>
                                                    </div>

                                                </div>
                                                @if(isset($user->id))
                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                @endif

                                                <button type="submit"
                                                    class="btn btn-primary mt-4 pr-4 pl-4">Save</button>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection