@extends('layout.steve')

@section('content')

<main>
    <section class="confirm-purchase-area">
        <div class="container-fluid">
            <div class="accordion-wrapper">
                <div class="container-fluid">
                    <div class="vertical-tabs-wrapper">
                        <div class="row">
                            <div class="col-md-3">

                                @include('steve/account.verticalLinks')

                            </div>


                            <div class="col-md-9">

                                <div class="fade shadow rounded bg-white show active p-5">
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="mb-4">Re-schedule session!</h4>
                                        </div>
                                        <p id="err" style="color:red;"></p>
                                        <div class="col-12">
                                            <form id="meetingfrm" method="post" action="{{url('account/meeting/update')}}/{{ $meeting->id}}" enctype='multipart/form-data'>

                                                {{ csrf_field() }}

                                                @if(isset($editStatus))
                                                @method('POST')
                                                @endif


                                                @if(session()->has('message'))
                                                <div class="alert alert-success">
                                                    {{ session()->get('message') }}
                                                </div>
                                                @endif


                                                @foreach($errors->all() as $error)
                                                <li>{{$error}}</li>
                                                @endforeach

                                                <div class="row">

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="date">Date</label>
                                                            <input type="date" class="form-control" id="date"
                                                                name="date" placeholder="Enter date"
                                                                value="">
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group">
                                                            <label for="timeSlotId">Time</label>
                                                            <select class="form-control selectpicker" id="timeSlotId" name="timeSlotId" data-live-search="true">
                                            <option value="">Select Time</option>
                                            @if(isset($timeSlot))
                                            @foreach($timeSlot as $value)
                                            <option value="{{$value->id}}">{{date('h:i',strtotime($value->fromTime))}}-{{date('h:i',strtotime($value->toTime))}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(isset($meeting->id))
                                                <input type="hidden" name="id" value="{{ $meeting->id }}">
                                                <input type="hidden" name="type" value="reschedule">
                                                @endif

                                                <button type="submit"
                                                    class="btn btn-primary mt-4 pr-4 pl-4">Update</button>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script type="text/javascript">

$(document).ready(function(){

    $("#meetingfrm").submit(function(){

        if($("#date").val()=="")
        {
            $("#err").text("Please enter date");
            $("#date").focus();
            return false;
        }
        if($("#timeSlotId").val()=="")
        {
            $("#err").text("Please enter time slot");
            $("#timeSlotId").focus();
            return false;
        }
        });
    });
 
</script>
@endsection