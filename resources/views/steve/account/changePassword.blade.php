@extends('layout.steve')

@section('content')

<main>
    <section class="confirm-purchase-area">
        <div class="container-fluid">
            <div class="accordion-wrapper">
                <div class="container-fluid">
                    <div class="vertical-tabs-wrapper">
                        <div class="row">
                            <div class="col-md-3">

                                @include('steve/account.verticalLinks')

                            </div>


                            <div class="col-md-9">

                                <div class="fade shadow rounded bg-white show active p-5">

                                    <div class="row">
                                        <div class="col-12 mt-6">
                                            <h4 class="mb-4">Change Password</h4>
                                        </div>
                                         <p id="err" style="color:red;"></p>

                                        <div class="col-12 mt-6">
                                            @if (session('error'))
                                            <div class="alert alert-danger">
                                                {{ session('error') }}
                                            </div>
                                            @endif
                                            @if (session('success'))
                                            <div class="alert alert-success" id="alert">
                                                {{ session('success') }}
                                            </div>
                                            @endif

                                            <form class="form-horizontal" id="updatepassfrm" method="POST"
                                                action="{{ url('user/updatePassword') }}/{{$userId}}">
                                                {{ csrf_field() }}
                                                <div
                                                    class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                    <label for="new-password-confirm"
                                                        class="col-md-4 control-label">Password <span
                                                            class="required-field">*</span></label>
                                                    <div class="col-md-12">
                                                        <input id="current-password" type="password"
                                                            class="form-control" placeholder="Enter old password" name="current-password">
                                                        @if ($errors->has('current-password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('current-password') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                                    <label for="new-password-confirm" class="col-md-4 control-label">New
                                                        Password <span class="required-field">*</span></label>
                                                    <div class="col-md-12">
                                                        <input id="new-password" type="password" class="form-control" placeholder="Enter new password" 
                                                            name="new-password">
                                                        @if ($errors->has('new-password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('new-password') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="new-password-confirm"
                                                        class="col-md-4 control-label">Confirm
                                                        New Password <span class="required-field">*</span></label>
                                                    <div class="col-md-12">
                                                        <input id="new-password-confirm" type="password"
                                                            class="form-control" placeholder="Enter confirm password"  name="new-password_confirmation">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <button type="submit" onclick="test();"
                                                            class="btn btn-success">Change Password</button>
                                                        <!-- <button class="btn btn-info" onclick="window.history.go(-1); return false;">Cancel</button> -->
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script type="text/javascript">

$(document).ready(function(){

    $("#updatepassfrm").submit(function(){

        if($("#password").val()=="")
        {
            $("#err").text("Please enter password");
            $("#password").focus();
            return false;
        }
        if($("#new-password").val()=="")
        {
            $("#err").text("Please enter new password");
            $("#new-password").focus();
            return false;
        }
        if($("#new-password-confirm").val()=="")
        {
            $("#err").text("Please enter confirm password");
            $("#new-password-confirm").focus();
            return false;
        }
        });
    });
 
</script>
@endsection