@extends('layout.steve')

@section('content')

<main>
    <section class="confirm-purchase-area">
        <div class="container-fluid">
            <div class="accordion-wrapper">
                <div class="container-fluid">
                    <div class="vertical-tabs-wrapper">
                        <div class="row">
                            <div class="col-md-3">

                                @include('steve/account.verticalLinks')

                            </div>


                            <div class="col-md-9">

                                <div class="fade shadow rounded bg-white show active p-5">

                                    <div class="row">
                                        <div class="col-12 mt-6">
                                            <h4 class="mb-4">Manage Children</h4>
                                        </div>

                                        <div class="col-12 mt-6">
                                            <form method="post" id="deleteAllUser">
                                                <div class="data-tables table-responsive">
                                                    <table id="userTable" class="text-center table">
                                                        <thead class="bg-light text-capitalize table-primary">
                                                            <tr>
                                                                <th width="5%">Seq.</th>
                                                                <th>Name</th>
                                                                <th>Email</th>
                                                                <th>Role</th>
                                                                <th>Meeting Username</th>
                                                                <th>Meeting Password</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                            <tbody>
                                                @if(isset($users))
                                                @foreach($users as $value)
                                                <tr id="item{{$value->id}}">
                                                    <td> {{$loop->iteration}} </td>
                                                    <td>@isset($value->name){{$value->name}}@else NA @endif</td>
                                                    <td>@isset($value->email){{$value->email}}@else NA @endif</td>
                                                    <td>@isset($value->role->name){{$value->role->name}}@else NA @endif</td>
                                                    <td>@isset($value->meetingUsername){{$value->meetingUsername}}@else NA @endif</td>
                                                    <td>@isset($value->meetingPassword){{$value->meetingPassword}} @else NA @endif</td>
                                                    <td>
                                                        
                                                            <a class="btn btn-info"
                                                                href="{{route('account.edit', $value->id)}}">Edit</a>
                                                            <!-- <a class="dropdown-item"
                                                                onclick="deleteRecord('{{$value->id}}','Delete this User details?','Are you sure you want to delete this User details?');">Delete</a> -->
                                                       
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection