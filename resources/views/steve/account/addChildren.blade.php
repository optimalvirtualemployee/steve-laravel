@extends('layout.steve')

@section('content')

<main>
    <section class="confirm-purchase-area">
        <div class="container-fluid">
            <div class="accordion-wrapper">
                <div class="container-fluid">
                    <div class="vertical-tabs-wrapper">
                        <div class="row">
                            <div class="col-md-3">

                                @include('steve/account.verticalLinks')

                            </div>


    <div class="col-md-9">

        <div class="fade shadow rounded bg-white show active p-5">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-4">Add Children Account!</h4>
                </div>
                <p id="err" style="color:red;"></p>
                <div class="col-12">
                    <form id="userfrm" method="post"
                        action="@if(isset($editStatus)){{ route('account.update', $user->id) }} @else {{ route('account.store')}}@endif"
                        enctype='multipart/form-data'>

                        {{ csrf_field() }}

                        @if(isset($editStatus))
                        @method('PUT')
                        @endif


                        @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                        @endif


                        @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach

                        <div class="row">

                             <div class="col-12">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username"
                                        name="username" placeholder="Enter username"
                                        value="{{old('username',  isset($user->username) ? $user->username : NULL)}}" <?php if(isset($editStatus)){echo "readonly";} ?>>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name"
                                        name="name" placeholder="Enter name"
                                        value="{{old('name',  isset($user->name) ? $user->name : NULL)}}">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email"
                                        name="email" placeholder="Enter email"
                                        value="{{old('email',  isset($user->email) ? $user->email : NULL)}}" <?php if(isset($editStatus)){echo "readonly";} ?>>
                                </div>
                            </div>
                            @if(!isset($user->password))
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control" id="password"
                                        name="password" placeholder="Enter password"
                                        value="{{old('password',  isset($user->password) ? $user->password : NULL)}}">
                                </div>
                            </div>
                            @endif

                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group inputDnD">
                                    <label for="name">Thumbnail</label>
                                    <input type="file" name="image" class="form-control-file text-muted font-weight-bold">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            @if(isset($user->image->name))
                            <div class="col-6">
                                <div class="upload-image">
                                    <img width="100" height="60"
                                        src=" {{ URL::to('/') }}/uploads/user/{{ $user->image->name }}"
                                        alt="image">
                                </div>
                            </div>
                            @endif
                        </div>
                        @if(isset($user->id))
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        @endif

                        <button type="submit"
                            class="btn btn-primary mt-4 pr-4 pl-4">Save</button>
                    </form>
                </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script type="text/javascript">

$(document).ready(function(){

    $("#userfrm").submit(function(){

        if($("#username").val()=="")
        {
            $("#err").text("Please enter user name");
            $("#username").focus();
            return false;
        }
        if($("#name").val()=="")
        {
            $("#err").text("Please enter name");
            $("#name").focus();
            return false;
        }
        if($("#email").val()=="")
        {
            $("#err").text("Please enter email");
            $("#email").focus();
            return false;
        }
        if($("#password").val()=="")
        {
            $("#err").text("Please enter password");
            $("#password").focus();
            return false;
        }
        });
    });
 
</script>
@endsection