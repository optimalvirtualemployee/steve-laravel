<!-- Vertiacal Links -->
<div class="nav flex-column nav-pills nav-pills-custom" aria-orientation="vertical">

    <a class="nav-link mb-3 p-3 shadow @if(isset($activeMenu)) @if($activeMenu=='dashboard' ) active @endif @endif" href="{{ url('/account/dashboard') }}">

        <i class="fa fa-home mr-2"></i>
        <span class="font-weight-bold small text-uppercase">Dashboard</span></a>

    <a class="nav-link mb-3 p-3 shadow shadow @if(isset($activeMenu)) @if($activeMenu=='account-manage-session' ) active @endif @endif"
        href="{{ url('/account/manage-session') }}">
        <i class="fa fa-calendar mr-2"></i>

        <span class="font-weight-bold small text-uppercase">Manage Session</span></a>

    <a class="nav-link mb-3 p-3 shadow shadow @if(isset($activeMenu)) @if($activeMenu=='account-manage-order' ) active @endif @endif"
        href="{{ url('/account/order') }}">
        <i class="fa fa-folder-open mr-2"></i>

        <span class="font-weight-bold small text-uppercase">My Order</span></a>

    <a class="nav-link mb-3 p-3 shadow @if(isset($activeMenu)) @if($activeMenu=='account-profile' ) active @endif @endif"
        href="{{ url('/account/profile') }}">

        <i class="fa fa-id-badge mr-2"></i>
        <span class="font-weight-bold small text-uppercase">Profile</span></a>

    <a class="nav-link mb-3 p-3 shadow action @if(isset($activeMenu)) @if($activeMenu=='change-password' ) active @endif @endif"
        href="{{ url('/account/change-password') }}">

        <i class="fa fa-key mr-2"></i>
        <span class="font-weight-bold small text-uppercase">Change Password</span></a>

        @if(isset(Auth::user()->roleId))
        @if(Auth::user()->roleId == 3)

    <a class="nav-link mb-3 p-3 shadow @if(isset($activeMenu)) @if($activeMenu=='account-manage-children') active @endif @endif"
        href="{{ url('/account/manage-children') }}">

        <i class="fa fa-tasks mr-2"></i>
        <span class="font-weight-bold small text-uppercase">Manage children</span></a>

    <a class="nav-link mb-3 p-3 shadow shadow @if(isset($activeMenu)) @if($activeMenu=='account-children-account' ) active @endif @endif"
        href="{{ url('/account/add-children') }}">
        <i class="fa fa-user-circle mr-2"></i>

        <span class="font-weight-bold small text-uppercase">Children Account</span></a>
        @endif
        @endif
</div>