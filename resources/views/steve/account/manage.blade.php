@extends('layout.steve')
@section('content')
<main>
   <section class="confirm-purchase-area">
      <div class="container-fluid">
         <div class="accordion-wrapper">
            <div class="container-fluid">
               <div class="vertical-tabs-wrapper">
                  <div class="row">
                     <div class="col-md-3">
                        @include('steve/account.verticalLinks')
                     </div>
                     <div class="col-md-9">
                        <div class="fade shadow rounded bg-white show active p-5">
                           <div class="row">
                              <div class="col-12 mt-6">
                                 <h4 class="mb-4">My Order</h4>
                              </div>
                              <div class="col-12 mt-6">
                                 <form method="post" id="deleteAllOrder">
                                    <div class="data-tables table-responsive">
                                       <table id="orderTable" class="table table-bordered">
                                          <thead class="bg-light text-capitalize table-primary">
                                             <tr>
                                                <th width="5%">Seq.</th>
                                                <th>Customer Name</th>
                                                <th>Customer Email</th>
                                                <th>Amount</th>
                                                <th>Order Status</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @foreach($order as $value)
                                             <tr id="item{{$value->id}}">
                                                <td> {{$loop->iteration}} </td>
                                                <td><a style="color: #2196F3;" href="{{route('account.show', $value->id)}}">{{$value->customerFirstName}} {{$value->customerLastName}} </a></td>
                                                <td>@if($value->customerEmail){{$value->customerEmail}}@else NA @endif</td>
                                                <td>@if($value->totalAmount){{$value->totalAmount}}@else NA @endif</td>
                                                <td>
                                                   @if($value->orderStatus == 1)
                                                   Pending
                                                   @elseif($value->orderStatus == 2)
                                                   Completed
                                                   @else
                                                   Hold
                                                   @endif
                                                </td>
                                             </tr>
                                             @endforeach
                                          </tbody>
                                       </table>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection