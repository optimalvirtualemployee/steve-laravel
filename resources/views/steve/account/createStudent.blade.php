@extends('layout.steve')

@section('content')

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<main>
    <section class="confirm-purchase-area">
    <div class="container-fluid">
       <div class="accordion-wrapper">
           <div class="container-fluid">
               <div class="vertical-tabs-wrapper">
                   <div class="row">
                        <div class="col-md-3">
                            <!-- Tabs nav -->
                            <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link mb-3 p-3 shadow active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                    <i class="fa fa-user-circle-o mr-2"></i>
                                    <span class="font-weight-bold small text-uppercase">Profile</span></a>
                               

                             
                                <a class="nav-link mb-3 p-3 shadow" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                    <i class="fa fa-calendar-minus-o mr-2"></i>
                                    <span class="font-weight-bold small text-uppercase">Change Password</span></a>

                                <a class="nav-link mb-3 p-3 shadow" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                    <i class="fa fa-star mr-2"></i>
                                    <span class="font-weight-bold small text-uppercase">Manage children</span></a>
                                <a class="nav-link mb-3 p-3 shadow" id="vs-pills-settings-tab" data-toggle="pill" href="#vs-pills-settings" role="tab" aria-controls="vs-pills-settings" aria-selected="false">
                                    <i class="fa fa-check mr-2"></i>
                                    <span class="font-weight-bold small text-uppercase">Children Account</span></a>
                                <a class="nav-link mb-3 p-3 shadow" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                    <i class="fa fa-check mr-2"></i>
                                    <span class="font-weight-bold small text-uppercase">Logout</span></a>

                                </div>
                        </div>

                        <div class="col-md-9">
                            <!-- Tabs content -->
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade shadow rounded bg-white show active p-5" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <div class="row">
                                         <div class="col-8 mt-6">
                                    <h4 class="font-italic mb-4">View/Update</h4>
                                </div>
                                @if(isset($user->image->name))
                                            <div class="col-4 mt-6">
                                                <div class="upload-image">
                                                    <img width="100" height="60"
                                                        src=" {{ URL::to('/') }}/uploads/user/{{ $user->image->name }}"
                                                        alt="image">
                                                </div>
                                            </div>
                                            @endif
                            </div>
                                </div>

                        <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <h4 class="font-italic mb-4">Change Password</h4>
                        
                                </div>

                                <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                    <h4 class="font-italic mb-4">Manage children</h4>
                                <form method="post" id="deleteAllUser">
                                <div class="data-tables">
                        <table id="userTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th width="5%">Seq.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($users))
                                @foreach($users as $value)
                                <tr id="item{{$value->id}}">
                                    <td> {{$loop->iteration}} </td>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->email}}</td>
                                    <td>{{$value->role->name}}</td>
                                    <td>
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                            Action
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('account.edit', $value->id)}}">Edit</a>
                                            <a class="dropdown-item" onclick="deleteRecord('{{$value->id}}','Delete this User details?','Are you sure you want to delete this User details?');">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </form>
                                </div>

                                <div class="tab-pane fade shadow rounded bg-white p-5" id="vs-pills-settings" role="tabpanel" aria-labelledby="vs-pills-settings-tab">
                                    <h4 class="font-italic mb-4">Account</h4>
                                    <form id="userfrm" method="post" action="@if(isset($editStatus)){{ route('account.update', $user->id) }} @else {{ route('account.store')}}@endif" enctype='multipart/form-data'>

                            {{ csrf_field() }}

                            @if(isset($editStatus))
                            @method('PUT')
                            @endif


                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif


                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach

                            <div class="row">

                                <div class="col-6 mt-5">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="{{old('name',  isset($user->name) ? $user->name : NULL)}}">
                                    </div>
                                </div>

                                <div class="col-6 mt-5">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{old('email',  isset($user->email) ? $user->email : NULL)}}">
                                    </div>
                                </div>
                            @if(!isset($user->password))
                                <div class="col-12 mt-5">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="text" class="form-control" id="password" name="password" placeholder="Enter Password" value="{{old('password',  isset($user->password) ? $user->password : NULL)}}">
                                    </div>
                                </div>
                            @endif    

                            </div>

                            <div class="row">
                             <div class="col-6 mt-6">
                                    <div class="form-group">
                                        <label for="name">Thumbnail</label>
                                        <input type="file" name="image" class="form-control">
                                    </div>
                                </div>
                           
                                  <div class="col-6 mt-5">
                                    <div class="form-group">
                                        <label for="roleId">Role - Student</label>
                                        <input type="checkbox" class="form-control" id="roleId" name="roleId" placeholder="Enter Password" value="4" checked="" >
                                    </div>
                                </div>
                            </div>
                             <div class="row">

                                @if(isset($user->image->name))
                                <div class="col-6 mt-6">
                                    <div class="upload-image">
                                        <img width="100" height="60" src=" {{ URL::to('/') }}/uploads/user/{{ $user->image->name }}" alt="image">
                                    </div>
                                </div>
                                @endif 


                            </div>
 


                            @if(isset($user->id))
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            @endif
                           
                            <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Save</button>
                        </form>
                                </div>
                        <div class="tab-pane fade shadow rounded bg-white p-5" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                    <h4 class="font-italic mb-4">Logout</h4>
                                    <a style="color: #2196F3;" href="{{ url('/logout/') }}">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
           </div>
       </div>
    </div>
</section>
</main>
@endsection