@extends('layout.steve')
@section('content')

<style type="text/css">
.btn-warning {
  pointer-events: none;
  cursor: default;
}
.btn-danger {
  background: red;
}
.btn-primary {
  background: blue;
}
</style>
<main>
   <section class="confirm-purchase-area">
      <div class="container-fluid">
         <div class="accordion-wrapper">
            <div class="container-fluid">
               <div class="vertical-tabs-wrapper">
                  <div class="row">
                     <div class="col-md-3">
                        @include('steve/account.verticalLinks')
                     </div>
                     <div class="col-md-9">
                        <div class="fade shadow rounded bg-white show active p-5">
                           <div class="row">
                              <div class="col-12 mt-6 p-0">
                                 <h4 class="mb-4">Manage Session</h4>
                              </div>

                              @if(session()->has('message'))
                                <div class="alert alert-success w-100">
                                    {{ session()->get('message') }}
                                </div>
                                @endif


                                @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                              <div class="col-12 mt-6 p-0">
                                
                                    <div class="data-tables table-responsive">
                                       <table id="userTable" class="text-center table">
                                          <thead class="bg-light text-capitalize table-primary">
                                             <tr>
                                                <th>Attend By</th>
                                                <th>Program</th>
                                                <th>Subject</th>
                                                <th>Topic</th>
                                                <th>State</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @if(isset($meeting))
                                             @foreach($meeting as $value)
                                             <tr id="item">
                                                <td> @isset($value->username->name){{$value->username->name}} @else NA @endif </td>
                                                <td> @isset($value->program->name){{$value->program->name}} @else NA @endif </td>
                                                <td> @isset($value->subject->name){{$value->subject->name}} @else NA @endif </td>
                                                <td> @isset($value->topic->title){{$value->topic->title}} @else NA @endif </td>
                                                <td> @isset($value->state->name){{$value->state->name}} @else NA @endif </td>
                                                <td> @isset($value->date){{$value->date}} @else NA @endif</td>
                                                <td> @isset($value->timeSlot->id)
                                                {{date('h:i',strtotime($value->timeSlot->fromTime))}}- {{date('h:i',strtotime($value->timeSlot->toTime))}}@else NA @endif </td>
                                                <td>


                                                   @if(($value->meetingStatus == 1 || $value->meetingStatus == 3) && $value->status == 1)

                                                   <?php if($value->meetingStatus != 3 ){ ?> 

                                                   <a class="btn btn-danger" href="{{ url('/meeting/webinar', $value->id) }}" target="_blank">
                                                   Launch
                                                   </a>
                                                   <?php } ?>

                                                   <form id="sessionMissedFrom" method="post" action="{{url('account/meeting/update-session-missed')}}/{{ $value->id}}">

                                                   {{ csrf_field() }}


                                                   <div class="checkbox">
                                                   <label><input type="checkbox" id="markSessionMissed" name="isSessionMissed" value="1" <?php if($value->meetingStatus == 3 ){ echo "checked"; }?>> Mark Session Missed</label>
                                                   </div>

                                                   </form>

                                                 

                                                   @else

                                                   @if($value->scheduleStatus == 1)
                                                   <a class="btn btn-primary" href="javascript:;" title="">
                                                   scheduled
                                                   </a>

                                                   
                                                   <a class="btn btn-info" href="{{ url('/account/re-schedule-session', ['id' => $value->id, 'userId' => Auth::guard('front')->user()->id]) }}">
                                                   Re-schedule
                                                   </a>


                                                   @else
                                                   <a class="btn btn-info" href="{{ url('/account/edit-session', ['id' => $value->id, 'userId' => Auth::guard('front')->user()->id]) }}">
                                                   schedule
                                                   </a>

                                                
                                                   @endif




                                                   @endif

                                                  
                                                   
                                                </td>
                                                <!--<td>-->
                                                <!--   <a class="btn btn-info" href="{{ url('/meeting/webinar', $value->id) }}">-->
                                                <!--   Webinar-->
                                                <!--   </a>-->
                                                <!--</td>-->
                                             </tr>
                                             @endforeach
                                             @endif
                                          </tbody>
                                       </table>
                                    </div>
                                
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>

<script type="text/javascript">

$(function(){

         $('#markSessionMissed').on('change',function(){
          
            $('#sessionMissedFrom').submit();
            });
});

</script>
@endsection

