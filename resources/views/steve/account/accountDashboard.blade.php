@extends('layout.steve')

@section('content')

<main>
    <section class="confirm-purchase-area">
        <div class="container-fluid">
            <div class="accordion-wrapper">
                <div class="container-fluid">
                    <div class="vertical-tabs-wrapper">
                        <div class="row">
                            <div class="col-md-3">

                                @include('steve/account.verticalLinks')

                            </div>


                            <div class="col-md-9">

                                <div class="fade shadow rounded bg-white show active p-5">

                                    <div class="row">
                                        <div class="col-8 mt-6">
                                            <h4 class="mb-4">Welcome {{auth()->user()->name}}!</h4>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection