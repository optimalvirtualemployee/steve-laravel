@extends('layout.steve')

@section('content')
<main>
	<section class="confirm-purchase-area">
	<div class="container">
		<div class="d-flex justify-content-md-center align-items-center h-100 my-login-page">
			<div class="card-wrapper">
				<div class="card fat">
									@if(session()->has('message'))
				<div class="alert alert-success mb-0">
					{{ session()->get('message') }}
				</div>
				@endif

				@if (!$errors->isEmpty())
				<div class="alert alert-danger">
					@foreach($errors->all() as $error)

					<p>{{$error}}</p>
					@endforeach
				</div>
				@endif

					<div class="card-body pt-1">
						<p id="err" style="color:red;text-align: center;"></p>
						<h4 class="card-title">Login</h4>
						<form id="loginfrm" method="POST" class="my-login-validation" action="/account/login" novalidate="">

							{{ csrf_field() }}
							<div class="form-group">
								<label for="email">E-Mail Address</label>
								<input id="email" type="email" id="email" class="form-control" name="email" value="" required autofocus>
								<div class="invalid-feedback">
									Email is invalid
								</div>
							</div>

							<div class="form-group">
								<label for="password">Password</label>
								<input id="password" type="password" id="password" class="form-control" name="password" required data-eye>
							    <div class="invalid-feedback">
							    	Password is required
						    	</div>
									<!-- <a href="forgot.html" class="float-right">
										Forgot Password?
									</a> -->
								
							</div>

							<!-- <div class="form-group">
								<div class="custom-checkbox custom-control">
									<input type="checkbox" name="remember_me" id="remember" class="custom-control-input">
									<label for="remember" class="custom-control-label">Remember Me</label>
								</div>
							</div> -->

							<div class="form-group m-0">
								<button type="submit" class="btn header-btn btn-block">Login</button>
							</div>
							<div class="mt-4 text-center">
								Don't have an account? <a href="{{ url('/register/') }}">Register Here!</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</main>
<script type="text/javascript">

$(document).ready(function(){

    $("#loginfrm").submit(function(){

        if($("#email").val()=="")
        {
            $("#err").text("Please enter email");
            $("#email").focus();
            return false;
        }
        if($("#password").val()=="")
        {
            $("#err").text("Please enter password");
            $("#password").focus();
            return false;
        }
        });
    });
 
</script>
@endsection