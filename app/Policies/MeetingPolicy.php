<?php

namespace App\Policies;

use App\User;
use App\Model\Admin\Meeting;
use Illuminate\Auth\Access\HandlesAuthorization;

class MeetingPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any meetings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
       //
    }

    /**
     * Determine whether the user can view the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */

    public function view(User $user, Meeting $meeting)
    {
        $addBy = $meeting->addBy;
        $sessionRecieverId = $meeting->sessionRecieverId;

        if($user->id != $addBy && $sessionRecieverId != $user->id){
        return false;
        }
        
       return true;
    }

    // public function viewIncludeTeacher(User $user, Meeting $meeting)
    // {
    //     $addBy = $meeting->addBy;
    //     $sessionRecieverId = $meeting->sessionRecieverId;
    //     $tutorId = $meeting->tutorId;

    //     if($user->id != $addBy && $sessionRecieverId != $user->id && $user->id != $tutorId){
    //     return false;
    //     }
        
    //    return true;
    // }

    /**
     * Determine whether the user can create meetings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function update(User $user, Meeting $meeting)
    {
        //
    }

    /**
     * Determine whether the user can delete the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function delete(User $user, Meeting $meeting)
    {
        //
    }

    /**
     * Determine whether the user can restore the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function restore(User $user, Meeting $meeting)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the meeting.
     *
     * @param  \App\User  $user
     * @param  \App\Meeting  $meeting
     * @return mixed
     */
    public function forceDelete(User $user, Meeting $meeting)
    {
        //
    }
}
