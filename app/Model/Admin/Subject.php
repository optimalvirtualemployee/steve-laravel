<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subject';

    // public function image()
    // {
    //     return $this->belongsTo('App\Model\Admin\Media', 'imageId', 'id');
    // }

    public function program()
    {
        return $this->belongsTo('App\Model\Admin\Program', 'programId', 'id');
    }

}