<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    public function image()
    {
        return $this->belongsTo('App\Model\Admin\Media', 'imageId', 'id');
    }
    public function role()
    {
        return $this->belongsTo('App\Model\Admin\Role', 'roleId', 'id');
    }
}