<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'program';

    public function image()
    {
        return $this->belongsTo('App\Model\Admin\Media', 'imageId', 'id');
    }

    public function subject()
    {
        return $this->hasMany('App\Model\Admin\Subject');
    }
}