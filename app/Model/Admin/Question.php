<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'question';

    public function image()
    {
        return $this->belongsTo('App\Model\Admin\Media', 'imageId', 'id');
    }

    public function category()
    {
        
        return $this->belongsTo('App\Model\Admin\QuestionCategory', 'questionCategoryId', 'id');
    }

    public function questionOption()
    {
       // echo "hi";die;
        return $this->belongsTo('App\Model\Admin\QuestionOption', 'questionId', 'id');
    }
}