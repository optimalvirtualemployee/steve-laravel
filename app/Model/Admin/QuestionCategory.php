<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class QuestionCategory extends Model
{
    protected $table = 'question_category';

    public function program()
    {
        return $this->belongsTo('App\Model\Admin\Program', 'programId', 'id');
    }
    public function subject()
    {
        return $this->belongsTo('App\Model\Admin\Subject', 'subjectId', 'id');
    }
    public function state()
    {
        return $this->belongsTo('App\Model\Admin\States', 'stateId', 'id');
    }
    
}