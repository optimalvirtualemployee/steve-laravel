<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = 'topic';

    public function subject()
    {
        return $this->belongsTo('App\Model\Admin\Subject', 'subjectId', 'id');
    }

    public function program()
    {
        return $this->belongsTo('App\Model\Admin\Program', 'programId', 'id');
    }


}