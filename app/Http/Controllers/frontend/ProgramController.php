<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Program;
use App\Model\Admin\Contact;
use View;

class ProgramController extends Controller
{

  public function __construct()
  {
    //its just a dummy data object.
    $program = Program::where('status', 1)->orderBy('sortOrder')->get();

    // Sharing is caring
    View::share('program', $program);
}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = array();

        if($id){
            
        //get program by class
        $data['programs'] = Program::where('slug',$id)->where('status', 1)->first();
        $data['allPrograms'] = Program::where('status', 1)->get();
        $data["pageTitle"] = 'Program Details';
        return view('steve.pages.program-details')->with($data);

        }else{
            return View::make('errors.404');
        }
    }

    public function index1($id)
    {
        $data = array();
        //get program by class wise
        $data['programs'] = Program::where('slug',$id)->where('status', 1)->first();
        $data['allPrograms'] = Program::where('status', 1)->get();
        $data["pageTitle"] = 'Program Details';
        return view('steve.pages.program-details-2')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newsLetter(Request $request)
    {

        $rules = [
            'email' => 'required|email|unique:contact',
        ];
        $customMessages =[];
        $this->validate($request, $rules, $customMessages);

        $contact = new Contact();
        $contact->email = $request->input('email');
        $contact->status = 1;
        $contact->sortOrder = 1;
        $contact->increment('sortOrder');
        $contact->save();
        
        return back()->with('message', 'news letter successfully submitted!');


    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
