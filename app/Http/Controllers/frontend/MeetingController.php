<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin\SessionContant;
use App\Model\Admin\Program;
use App\Model\Admin\Meeting;
use App\Model\Admin\QuestionCategory;
use App\Model\Admin\Question;
use App\Model\Admin\QuestionOption;
use View;
use DB;
class MeetingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:front');
        $this->middleware(function ($request, $next) {
            $this->userId = Auth::user()->id;
            //$this->accountId = Auth::user()->accountId;
            return $next($request);
        });

        $program = Program::where('status', 1)->orderBy('sortOrder')->get();
        View::share('program', $program);   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $data = array();
        $data["meeting"] = Meeting::findOrFail($id);

        $this->authorize('view', $data["meeting"]);


        $data['questionCat'] = QuestionCategory::orderBy('id','DESC')->get();
        $i=1;
        $questionCategoryId = array();
            foreach ($data["questionCat"] as $key => $value) {
                $questionCategoryId[$i] = json_decode(json_encode($value->id), true);
                 $i++;
        }
        $data['question'] = Question::whereIn('questionCategoryId',$questionCategoryId)->orderBy('id','ASC')->get();

        $data['questionOption'] = QuestionOption::orderBy('id','ASC')->get();
       
        $data["sessionContant"] = SessionContant::where('sessionId',$id)->get();
        $data["pageTitle"] = 'Webinar';
        return view('steve.meeting.webinar')->with($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
