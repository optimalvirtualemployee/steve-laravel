<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\User;
use App\Model\Admin\Role;
use App\Model\Admin\Order;
use App\Model\Admin\OrderItem;
use App\Model\Admin\Program;
use App\Model\Admin\Subject;
use App\Model\Admin\Topic;
use App\Model\Admin\Plan;
use App\Model\Admin\TimeSlot;
use App\Model\Admin\States;
use App\Model\Admin\Meeting;
use View;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Mail;

use Redirect;


class BookSessionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:front');
        $this->middleware(function ($request, $next) {
            $this->userId = Auth::user()->id;
           // $this->accountId = Auth::user()->accountId;
            return $next($request);
        });

        $program = Program::where('status', 1)->orderBy('sortOrder')->get();
        View::share('program', $program);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();

        $userId = Auth::user()->id;
        $data["user"] = User::where('parentId',$userId)->where('roleId','4')->orderBy('sortOrder')->get();


        $data["program"] = Program::where('status',1)->orderBy('sortOrder')->get();
        $data["state"] = States::where('status',1)->orderBy('sortOrder')->get();
        $data["timeSlot"] = TimeSlot::where('status',1)->orderBy('sortOrder')->get();

        $data["pageTitle"] = 'Manage Book Session';
        return view('steve.bookSession.book-session')->with($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $this->validate(request(), [
        //     'customerFirstName' => 'required',
        //     'customerLastName' => 'required',
        //     'customerEmail' => 'required',
        //     'customerContact' => 'required',
        // ],
        // [
        //     'customerFirstName.required'=> 'Fisrt name is required',
        //     'customerLastName.required'=> 'Last name is required',
        //     'customerEmail.required'=> 'Email is required',
        //     'customerContact.required'=> 'Contact number is required',
        // ]);

        
        
        $order = new Order();
             
        $getId = auth()->user();
        $userId = $getId->id;

        $order->userId = $userId;

        if($request->input('userId')){
        $order->sessionRecieverId = $request->input('userId');
        }else{
        $order->sessionRecieverId = $userId;   
        }

       
        
        $order->customerFirstName     = $request->input('firstName');
        $order->customerLastName = $request->input('lastName');
        $order->customerEmail = $request->input('email');
        $order->customerContact = $request->input('contact');
        $order->billingState = $request->input('state');
        $order->billingCountry = $request->input('country');
        $order->billingPostCode = $request->input('postCode');
        $order->totalAmount = $request->input('amount');
        $order->status = 1;
        $order->sortOrder = 1;
        $order->increment('sortOrder');
        $order->save();

        $orderId = $order->id;

        
        
        if($orderId){

            // Inserting Order Items

        $this->validate(request(), [
            'programId' => 'required',
            'subjectId' => 'required',
            'topicId' => 'required',
            'stateId' => 'required',
            'noOfSessionn' => 'required',
            'amount' => 'required',

        ],
        [
            'programId.required'=> 'Program is required',
            'subjectId.required'=> 'Subject is required',
            'topicId.required'=> 'Topic is required',
            'stateId.required'=> 'State is required',
            'noOfSessionn.required'=> 'No of Session is required',
            'amount.required'=> 'Amount is required',

        ]);

        
        
        $orderItem = new OrderItem();
             
        $orderItem->orderId = $orderId;
        $orderItem->programId     = $request->input('programId');
        $orderItem->subjectId = $request->input('subjectId');
        $orderItem->topicId = $request->input('topicId');
        $orderItem->timeSlotId = $request->input('timeSlotId');
        $orderItem->stateId = $request->input('stateId');
        $orderItem->noOfSessionn = $request->input('noOfSessionn');
        $orderItem->amount = $request->input('amount');
        $orderItem->status = 1;
        $orderItem->sortOrder = 1;
        $orderItem->increment('sortOrder');
        $orderItem->save();

        
         // Inserting Meetings for order
         $this->insertMeeting($orderId, $order->sessionRecieverId);
         
         

    }
        return Redirect::to('payment/'.$orderId);

       // return $this->proceedPayment($orderId);

        //return redirect('/book-session')->with('message', 'Session Booked Successfully');
    }
    
    //payment page
    public function proceedPayment($orderId)
    {

        $data1 = array();

        $url = "https://hello.sandbox.auspost.com.au/oauth2/ausujjr7T0v0TTilk3l5/v1/token";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "Authorization: Basic MG9heGI5aThQOXZRZFhUc24zbDU6MGFCc0dVM3gxYmMtVUlGX3ZEQkEySnpqcENQSGpvQ1A3b0k2amlzcA==",
        "Content-Type: application/x-www-form-urlencoded",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = "grant_type=client_credentials&scope=https%3A%2F%2Fapi.payments.auspost.com.au%2Fpayhive%2Fpayments%2Fread%20https%3A%2F%2Fapi.payments.auspost.com.au%2Fpayhive%2Fpayments%2Fwrite%20https%3A%2F%2Fapi.payments.auspost.com.au%2Fpayhive%2Fpayment-instruments%2Fread%20https%3A%2F%2Fapi.payments.auspost.com.au%2Fpayhive%2Fpayment-instruments%2Fwrite";

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
       $result = json_decode($resp, true); // true turns it into an array
       $access_token = $result['access_token'];
       $data1['code'] = $access_token;
       $data1['orderId'] = $orderId;


        return view('steve.pages.payment')->with($data1);

    }

    public function insertMeeting($orderId, $sessionRecieverId)
    {
        
        
        $orderItem = OrderItem::where('orderId', $orderId)->get();

        foreach($orderItem as $value){

        $length = $value->noOfSessionn; 

        for($i = 1; $i <= $length; $i++){

        $meeting = new Meeting();
        $meeting->programId = $value->programId;
        $meeting->subjectId = $value->subjectId;
        $meeting->topicId = $value->topicId;
       // $meeting->tutorId = $value->subjectId;
        $meeting->sessionRecieverId = $sessionRecieverId;
        $meeting->addBy = $this->userId;
        $meeting->stateId = $value->stateId;
        // $meeting->date = $request->input('date');
        // $meeting->timeSlotId = $request->input('timeSlotId');
        // $meeting->name = $request->input('name');
        // $meeting->description = $request->input('description');
        // $meeting->practiceSetQuestionId = $request->input('practiceSetQuestionId');  
        $meeting->status = 1;
        $meeting->sortOrder = 1;
        $meeting->increment('sortOrder');
        $meeting->save();

        }

        }
        
        
        //Mail::to(env('MAIL_FROM_ADDRESS','pashu.sharma@gmail.com'))->send(new WelcomeMail());
        
        
          Mail::send('emails.welcome',  ['meeting' =>  $meeting], function ($m){
            $m->from('hello@app.com', 'Your Application');
            
            $m->to(['priyank@optimalvirtualemployee.com', 'pashu.sharma@gmail.com'])->subject('Session Registered Succesfully!');
        });  
    
    }

    public function getBookSubjectByProgramId(Request $request)
    {

        $programId = $request->programId;
        $subject = Subject::where('programId', $programId)->where('status', 1)->get();
        

        $html = '<option value="">Select Subject</option>';

        foreach ($subject as $value) {
            $html .= "<option value='" . $value->id . "'>" . $value->name . "</option>";
        }

        if (!empty($html)) {
            $response = array('status' => 1, 'message' => 'Status updated', 'response' => $html);
        } else {
            $response = array('status' => 0, 'message' => 'Something went wrong', 'response' => '');
        }

        return response()->json($response);
    }
        public function getBookTopicBySubjectId(Request $request)
    {
        $subjectId = $request->subjectId;
        $subject = Topic::where('subjectId', $subjectId)->where('status', 1)->get();

        $html = '<option value="">Select Topic</option>';

        foreach ($subject as $value) {
            $html .= "<option value='" . $value->id . "'>" . $value->title . "</option>";
        }

        if (!empty($html)) {
            $response = array('status' => 1, 'message' => 'Status updated', 'response' => $html);
        } else {
            $response = array('status' => 0, 'message' => 'Something went wrong', 'response' => '');
        }

        return response()->json($response);
    }

        public function getTeacherByProgramIDSubjectId(Request $request)
    {
        $programId = $request->programId;
        

        $subject = TeacherClass::where('programId', $programId)->first();

        $subjectId = $subject->subjectId;

        $subject = TeacherClass::where('programId', $programId)->where('subjectId', $subjectId)->get();

        $html = '<option value="">Select Teacher</option>';

        foreach ($subject as $value) {
            $html .= "<option value='" . $value->teacher->id . "'>" . $value->teacher->name . "</option>";
        }

        if (!empty($html)) {
            $response = array('status' => 1, 'message' => 'Status updated', 'response' => $html);
        } else {
            $response = array('status' => 0, 'message' => 'Something went wrong', 'response' => '');
        }

        return response()->json($response);
    }


    public function getPlanByTopicId(Request $request)
    {
        $topicId = $request->topicId;
        

        $plan = Plan::where('topicId', $topicId)->get();


        $html = '<option value="">Select Quantity</option>';

        foreach ($plan as $value) {
            $html .= "<option value='" . $value->quantity . "'>" . $value->quantity . "</option>";
        }

        if (!empty($html)) {
            $response = array('status' => 1, 'message' => 'Status updated', 'response' => $html);
        } else {
            $response = array('status' => 0, 'message' => 'Something went wrong', 'response' => '');
        }

        return response()->json($response);
    }


    public function getPlanByTopicQty(Request $request)
    {
        $topicId = $request->topicId;
        $qty = $request->qty;
        $plan = Plan::where('topicId', $topicId)->where('quantity', $qty)->where('status', 1)->first();

       
        if (!empty($plan)) {
            $response = array('status' => 1, 'message' => 'Status updated', 'response' => $plan);
        } else {
            $response = array('status' => 0, 'message' => 'Something went wrong', 'response' => '');
        }

        return response()->json($response);
    }


}
