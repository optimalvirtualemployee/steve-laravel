<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\User;
use App\Model\Admin\Role;
use App\Model\Admin\Meeting;
use App\Model\Admin\TimeSlot;
use App\Model\Admin\Order;
use App\Model\Admin\OrderItem;
use App\Model\Admin\Program;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use View;


class AccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:front');
        $this->middleware(function ($request, $next) {
            
            $this->userId = Auth::guard('front')->user()->id;
           // $this->accountId = Auth::guard('front')->user()->accountId;
            return $next($request);
        });

    $program = Program::where('status', 1)->orderBy('sortOrder')->get();
    View::share('program', $program);
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = array();
        $userId = $this->userId;
        $data["order"] = Order::where('userId',$userId)->orderBy('sortOrder')->get();
        $data["pageTitle"] = 'My Order';
        $data["activeMenu"] = 'account-manage-order';
        return view('steve.account.manage')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

        $data = array();

        $data['order'] = Order::find($id);
        $data["meeting"] = Meeting::find($id);

        if ($data["order"] == null) {

        return View::make('errors.404');

        }else {

        $data['orderItem'] = OrderItem::where('orderId', $id)->get();
        return view('steve.account.show')->with($data);
       }   
    }

    /**
    * User Profile.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function accountProfile()
    {
        $data['user'] = auth()->user();
        $data["editStatus"] = 1;
        $data["activeMenu"] = 'account-profile';
        return view('steve.account.accountProfile')->with($data);
    }

     /**
    * Account Chnage Password Page.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function changePassword()
    {

        $data['userId'] = $this->userId;

        $data["editStatus"] = 1;
      
        $data["activeMenu"] = 'change-password';
        return view('steve.account.changePassword')->with($data);
    }

        /**
     * Update User Password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updatePassword(Request $request, $id)
    {
        $user = User::find($id);

        if (!(Hash::check($request->get('current-password'), $user->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user($id);
        $user->password = $request->get('new-password');
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

    }

    /**
    * Manage Children Listing.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function manageChildren()
    {
        
        $accountId = $this->userId;
        $data["editStatus"] = 1;
        $data["users"] = User::where('parentId',$accountId)->where('roleId','4')->orderBy('sortOrder')->get();

        $data["activeMenu"] = 'account-manage-children';
        return view('steve.account.manageChildren')->with($data);
    }

    /**
    * Add Children Page.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function addChildren()
    {

        $data["activeMenu"] = 'account-children-account';
        return view('steve.account.addChildren')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {

        $this->validate(request(), [
            'username' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required',
            'email' => 'required|unique:users',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ],
        [
            'name.required'=> 'User name is required',
            'role.required'=> 'User role is required',
        ]);

        $user = new User();

        //Image Uploading

        if ($request->hasFile('image')) {  // Check if file input is set

            $mediaId = imageUpload($request->image, $user->imageId, $this->userId, "uploads/user/"); //Image, ReferenceRecordId, UserId, Path
            
            $user->imageId = $mediaId;
 
         }
        $userId = auth()->user();
        $accountId = $userId->id;
        
        $user->parentId = $accountId;
        $user->username = $request->input('username');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->roleId = 4;
        $user->status = 1;
        $user->sortOrder = 1;

        $user->increment('sortOrder');

        $user->save();

        return redirect('account/add-children')->with('message', 'User Added Successfully');
    }


    /**
    * Account Manage Session Page.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function manageSession(Request $request)
    {
        
        $userId = $this->userId;

        $data["editStatus"] = 1;
        $data["users"] = User::where('parentId',$userId)->where('roleId','4')->orderBy('sortOrder')->get();
       
        $i=1;
        $sessionRecieverId = array();
        $sessionRecieverId[0] = $userId;

        foreach ($data["users"] as $key => $value) {

            $sessionRecieverId[$i]=json_decode(json_encode($value->id), true);
            $i++;
        }

        //dd($sessionRecieverId);
        
        $data["meeting"] = Meeting::whereIn('sessionRecieverId', $sessionRecieverId)->orderBy('sortOrder')->get();

        $data["activeMenu"] = 'account-manage-session';

        return view('steve.account.manageSession')->with($data);
        }

    /**
    * Account Edit Session.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function editSession($id,$userId)
    { 
        $data = array();
        $data["meeting"] = Meeting::findOrFail($id);

        $this->authorize('view', $data["meeting"]);

        //checking for schedule Status
        if($data["meeting"]->scheduleStatus == 1){
         return redirect('/account/manage-session');
        }

        $data["timeSlot"] = TimeSlot::where('status',1)->orderBy('sortOrder')->get();
        $data["editStatus"] = 1;
        return view('steve.account.editSession')->with($data);

    }


    /**
    * Account Re-schedule Session.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function reScheduleSession($id,$userId)
    { 
        $data = array();
        $data["meeting"] = Meeting::findOrFail($id);

        $this->authorize('view', $data["meeting"]);

        //checking for schedule Status
        if($data["meeting"]->scheduleStatus != 1){
         return redirect('/account/manage-session');
        }

        $data["timeSlot"] = TimeSlot::where('status',1)->orderBy('sortOrder')->get();
        $data["editStatus"] = 1;
        return view('steve.account.reScheduleSession')->with($data);

    }




    

    /**
    * Update Meeting.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function updateMeeting(Request $request)
    {

        $this->validate(request(), [
            'date' => 'required',
            'timeSlotId' => 'required',
        ]);
        
        $id = $request->input('id');
        $meeting = Meeting::find($id);

        // checking uniqueness

        $sessionReciever = $meeting->sessionRecieverId;
        $date = $request->input('date');
        $timeSlotId = $request->input('timeSlotId');

        if($request->filled('type')) {

        $type = $request->input('type');
        if($type == 'reschedule'){

            $meeting->tutorId = NULL;
        }

        }

         $recordData = $this->checkUniqueScheduleForUserAccount($sessionReciever, $date, $timeSlotId);

         if($recordData){

            return redirect('/account/manage-session')->with('message', 'Same Timings are already scheduled for another session');
            die();

         }

        $meeting->date = $request->input('date');
        $meeting->timeSlotId = $request->input('timeSlotId');
        $meeting->scheduleStatus = 1;

        
        $meeting->save();

        return redirect('/account/manage-session')->with('message', 'Session Updated Successfully');
    }


    public function updateMeetingMissed(Request $request)
    {

       
        $isSessionMissed = $request->input('isSessionMissed');
        $id = $request->id;

       
        $meeting = Meeting::find($id);

        if($isSessionMissed == 1){

        $meeting->meetingStatus = 3;

        }else{

            $meeting->meetingStatus = 1;

        }
       

        $meeting->save();

        return redirect('/account/manage-session')->with('message', 'Session Updated Successfully');
    }

     /**
    * Checking Unique Schedule For User.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function checkUniqueScheduleForUserAccount($sessionReciever, $date, $timeSlotId)
    {
        $meeting = Meeting::where('sessionRecieverId', $sessionReciever)->where('date', $date)->where('timeSlotId', $timeSlotId)->first();

        return $meeting;
    }

    /**
    * Account User Edit Page.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function edit($id)
    {
        
        $data = array();

      
        $accountId = $this->userId;

        $data["users"] = User::where('parentId',$accountId)->where('roleId','4')->orderBy('sortOrder')->get();
        $data['user'] = User::find($id);

        $data["editStatus"] = 1;
        $data["pageTitle"] = 'Update Blog';
        $data["activeMenu"] = 'website-management';
        $data["activeSubMenu"] = 'blog';
        return view('steve.account.addChildren')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {

        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required',
        ]);
        
        $id = $request->input('id');

        $user = User::find($id);

         // Image Uploading

         if ($request->hasFile('image')) {  // Check if file input is set

            $mediaId = imageUpload($request->image, $user->imageId, $this->userId, "uploads/user/"); //Image, ReferenceRecordId, UserId, Path
            
            $user->imageId = $mediaId;
 
         }
        $user->username = $request->input('username'); 
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return redirect('/account/manage-children')->with('message', 'User Updated Successfully');
    }

    /**
    * Account Dashboard.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function accountDashboard()
    {
        $data["pageTitle"] = 'account Dashboard';
        $data["activeMenu"] = 'dashboard';
        return view('steve.account.accountDashboard')->with($data);
    }

    /**
     * Update User Profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function accountProfileUpdate(Request $request)
    {
        $id = $this->userId;
 
        $user = User::find($id);

        
        if ($request->hasFile('image')) {  // Check if file input is set

            $mediaId = imageUpload($request->image, $user->imageId, $this->userId, "uploads/user/"); //Image, ReferenceRecordId, UserId, Path
            
            $user->imageId = $mediaId;
 
         }
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        
        return redirect('/account/profile')->with('message', 'Profile Updated Successfully');
    }

}