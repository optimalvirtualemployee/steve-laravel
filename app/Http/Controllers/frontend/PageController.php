<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Model\Admin\Page;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin\Program;
use View;

use Session;


class PageController extends Controller
{
    public function __construct()
    {

        $program = Program::where('status', 1)->orderBy('sortOrder')->get();
        View::share('program', $program);   
    }

    public function page($slug)
    {
        $data['page'] = Page::where('slug', $slug)->first();

        if(!$data['page']){

            return abort(404);

        }

        return view('steve.pages.page')->with($data);
    }  
    
    public function paymentSuccess()
    {
        $data = '';

        return view('steve.pages.paymentSuccess')->with($data);
    }  

}