<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Model\Admin\User;
use App\Model\Admin\Meeting;

class CronController extends Controller
{
    
    public function teacherEarlyMorningReminder(){
        
        $currentDate = date('Y-m-d');
        
        $meetings = Meeting::where('date', $currentDate)->where('tutorId','!=', 'null')->where('meetingStatus','1')->get();
        
        foreach ($meetings as $meeting){
        
            $user = User::where('id', $meeting->tutorId)->first();
            
            Mail::send('emails.teacherEarlyMorningReminder',  ['meeting' =>  $meeting], function ($m){
                $m->from('hello@app.com', 'Your Application');
                
                $m->to(['priyank@optimalvirtualemployee.com'])->subject('Upcoming Sesssion Information!');
            });
            
        }
    }

    public function sendToStudentBeforeOneHour(){

        $currentDate = date('Y-m-d');
        
        
        $meetings = Meeting::where('date', $currentDate)
        ->where('tutorId','!=', 'null')
        ->where('meetingStatus','1')->with('timeSlot')->get();
        
        foreach ($meetings as $meeting){

            // Date time for the session to start

            
            $dateTime = $meeting->date .' '. date('H:i',strtotime($meeting->timeSlot->fromTime));
            $sessionTime = date('H',strtotime($meeting->timeSlot->fromTime));
            //$sessionTimeAfterOneHour = date('H',strtotime($meeting->timeSlot->fromTime.'+1 hours'));
            $timeAfterOneHour = date('H', strtotime('+1 hours'));

            // print_r($sessionTime);

            if($timeAfterOneHour == $sessionTime){
                    
                
            $user = User::where('id', $meeting->sessionRecieverId)->first();
            
            Mail::send('emails.sendToStudentBeforeOneHour',  ['meeting' =>  $meeting], function ($m){
              
                $m->from('hello@app.com', 'Your Application');
                
                //email from $user
                $m->to(['priyank@optimalvirtualemployee.com'])->subject('Upcoming Sesssion Information!');
            });
            
            
                $teacher = User::where('id', $meeting->tutorId)->first();

                
                Mail::send('emails.sendToTeacherBeforeOneHour',  ['meeting' =>  $meeting], function ($m){
                    
                    $m->from('hello@app.com', 'Your Application');
                    
                    //email from $teacher
                    $m->to(['priyank@optimalvirtualemployee.com'])->subject('Upcoming Sesssion Information!');
                });
        }
                  
        }
    }
}
