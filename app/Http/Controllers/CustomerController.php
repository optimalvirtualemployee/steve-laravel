<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator, Redirect, Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin\Order;
use App\Model\Admin\Customer;
use App\Model\Admin\GeneralSetting;
use App\Model\Admin\OrderBilling;
use App\Model\Admin\OrderItem;
use Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect as FacadesRedirect;
use Session;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            $this->customerData = $request->session()->get('customerSession');

            if(!isset($this->customerData->id)){
            Redirect::to('login')->send();
            }
            return $next($request);
        });
    }

    

    public function account()
    { 
       
        $data['customer'] = $this->customerData;

        $data["order"] = Order::where('customerId', $data['customer']->id)->orderBy('sortOrder')->get();

        $data["pageTitle"] = 'Account';
        return view('frontened.account')->with($data);
    }

    public function orderDetail($id)
    {
        $data['order'] = Order::find($id);
        $data['orderBilling'] = OrderBilling::where('orderId', $id)->first();
        $data['orderItem'] = OrderItem::where('orderId', $id)->get();
        $data['generalSetting'] = GeneralSetting::find(1);
        
        
        $data["pageTitle"] = 'Order detail';
        return view('frontened.orderDetail')->with($data);
    }


}
