<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Auth;
Use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:front')->except('logout');
    }

    //    public function showFrontLoginForm()
    //    {
          
    //     return view('steve.login', ['url' => 'front']);
    //    }

    // public function frontLogin(Request $request)
    // {
       
    //     $this->validate(request(), [
    //         'email' => 'required',
    //         'password' => 'required',
    //     ]);
        
    //     $credentials = $request->only('email', 'password');


    //     if (Auth::guard('front')->attempt($credentials)) {

    //         $user = Auth::guard('front')->user();
    //             if ($user->roleId === 3 || $user->roleId === 4) {
    //                 return redirect()->intended('account/dashboard');
                    
    //             }else{

    //                 Auth::guard('front')->logout(); 
    //                 return redirect()->route('login')->with('message', 'Invalid Access');
                    
    //             }


           
    //         return redirect()->intended('parent/dashboard');
    //     }

    //     return Redirect::to("/login")->with('message', 'Opps! Invalid credentials');     
    // }




}
