<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Blog;
use Validator, Redirect, Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin\ShutterStyle;
use App\Model\Admin\ShutterType;
use App\Model\Admin\Recess;
use App\Model\Admin\Frame;
use App\Model\Admin\Customer;
use App\Model\Admin\Hinge;
use App\Model\Admin\Louvre;
use App\Model\Admin\Pannel;
use App\Model\Admin\Program;
use App\Model\Admin\Contact;
use App\Model\Admin\Testimonial;
use App\Model\Admin\Banner;
use App\Model\Admin\Order;
use App\Model\Admin\OrderBilling;
use App\Model\Admin\OrderItem;
use DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Model\Admin\ProductDimension;
use App\Model\Admin\Tiltrod;
use View;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
     public function __construct()
  {
    //its just a dummy data object.
    $program = Program::where('status', 1)->orderBy('sortOrder')->get();

    // Sharing is caring
    View::share('program', $program);
}


public function frontLogin(Request $request)
{
   
    $this->validate(request(), [
        'email' => 'required',
        'password' => 'required',
    ]);
    
    $credentials = $request->only('email', 'password');


    if (Auth::guard('front')->attempt($credentials)) {

        $user = Auth::guard('front')->user();
            if ($user->roleId === 3 || $user->roleId === 4) {
                return redirect()->intended('account/dashboard');
                
            }else{

                Auth::guard('front')->logout(); 
                return redirect()->route('user.login')->with('message', 'Invalid Access');
                
            }


       
        return redirect()->intended('parent/dashboard');
    }

    return Redirect::to("/login")->with('message', 'Opps! Invalid credentials');     
}

public function showFrontLoginForm()
       {

        if (Auth::guard('front')->user()) {   // Check is user logged in

            return redirect()->intended('account/dashboard');
            
        }
          
        return view('steve.login', ['url' => 'front']);
       }


    public function index()
    {
      
        $data["banner"] = Banner::where('type', 1)->orderBy('sortOrder')->first();

        $data["testimonial"] = Testimonial::where('status', 1)->orderBy('sortOrder')->get();
        $data["pageTitle"] = 'Home';
        return view('steve.home')->with($data);
    }

    public function configurator()
    {
        
        $data["shutterStyle"] = ShutterStyle::orderBy('sortOrder')->get();
        $data["shutterType"] = ShutterType::orderBy('sortOrder')->get();
        $data["recess"] = Recess::orderBy('sortOrder')->get();
        $data["frame"] = Frame::orderBy('sortOrder')->get();
        $data["panel"] = Pannel::orderBy('sortOrder')->get();
        $data["louvre"] = Louvre::orderBy('sortOrder')->get();
        $data["tiltrod"] = Tiltrod::orderBy('sortOrder')->get();
        $data["hinge"] = Hinge::orderBy('sortOrder')->get();

        $data['type'] = 'configurator';

        $data["pageTitle"] = 'Configurator';
        return view('frontened.configurator')->with($data);
    }

    public function quickQuote(Request $request)
    {
        $data['width'] = $request->width;
        $data['height'] = $request->height;

        $data["shutterStyle"] = ShutterStyle::orderBy('sortOrder')->get();
        $data["shutterType"] = ShutterType::orderBy('sortOrder')->get();
        $data["recess"] = Recess::orderBy('sortOrder')->get();
        $data["frame"] = Frame::orderBy('sortOrder')->get();
        $data["panel"] = Pannel::orderBy('sortOrder')->get();
        $data["louvre"] = Louvre::orderBy('sortOrder')->get();
        $data["tiltrod"] = Tiltrod::orderBy('sortOrder')->get();
        $data["hinge"] = Hinge::orderBy('sortOrder')->get();

        $data['type'] = 'quickQuote';

        $data["pageTitle"] = 'Quick Quote';
        return view('frontened.configurator')->with($data);
    }

    public function register()
    {

        $data["pageTitle"] = 'Register';
        return view('steve.register')->with($data);
    }

    public function forgotPassword()
    {

        $data["pageTitle"] = 'forgot password';
        return view('steve.forgot-password')->with($data);
    }
    
    public function account()
    {

        $data["pageTitle"] = 'Account';
        return view('frontened.account')->with($data);
    }


    public function storeRegistration(Request $request)
    {

        $rules = [
            'roleId' => 'required',
            'username' => 'required|unique:users',
            'fullname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ];
        
        $customMessages =[];
        $this->validate($request, $rules, $customMessages);

        $user = new User();

        $user->roleId = $request->input('roleId');
        $user->username = $request->input('username');
        $user->name = $request->input('fullname');
        $user->email = $request->input('email');
        $user->password = $request->input('password');

        $user->status = 1;
        $user->sortOrder = 1;
        $user->increment('sortOrder');

        $user->save();

        
        Mail::send('emails.userRegistration',  ['user' =>  $user], function ($m){
            $m->from('hello@app.com', 'Your Application');
            
            $m->to('priyank@optimalvirtualemployee.com')->subject('Registration!');
        });

        // Mail::send([], [], function ($message) use ($emailMessage, $pathToMailTemplate, $pathToLogo) {
        //     $message->to("post@stenolav-management.no")->subject("Applauce Subscription Lead");
        //     $message->from("info@applauce.bay20.com", "Applauce");
        //     // here comes what you want
        //     ///->setBody('Hi, welcome user!'); // assuming text/plain
        //     // or:
        //     $message->setBody($emailMessage, 'text/html'); // for HTML rich messages

        //     if (!empty($pathToMailTemplate)) {
        //         $message->attach($pathToMailTemplate, array());
        //     }

        //     if (!empty($pathToLogo)) {
        //         $message->attach($pathToLogo, array());
        //     }
        // });

        // Sending email

        
        return Redirect::to("/login")->with('message', 'Registration successfull');
    }


    public function userLogin(Request $request)
    {

        $this->validate(request(), [
            'email' => 'required',
            'password' => 'required',
        ]);


            $credentials = $request->only('email', 'password');
            
            if (Auth::guard('front')->attempt($credentials)) {

                $roleID = Auth::guard('front')->user()->roleId;
                if ($roleID == 3){
                    return redirect()->intended('parent/dashboard');
                }else if($roleID == 4){
                   return redirect()->intended('student/dashboard');
                }else{
                    echo "NO";
                }
                // Authentication passed...
                
            }
            return Redirect::to("/login")->with('message', 'Opps! Invalid credentials');       
    }

    public function logout()
    {
        Auth::guard('front')->logout(); 
        return Redirect::to("login")->with('message', 'Logged out successfully');
    }
    public function contactUs(Request $request){

        $rules = [
            'email' => 'required|email|unique:contact',
            'message' => 'required',
        ];
        $customMessages =[];
        $this->validate($request, $rules, $customMessages);

        $contact = new Contact();

        $contact->email = $request->input('email');
        $contact->message = $request->input('message');

        $contact->status = 1;
        $contact->sortOrder = 1;
        $contact->increment('sortOrder');
        $contact->save();
        return redirect()->route('home')->with('message', 'Contact successfully submitted');       

    }

    public function blog()
    {
        $data["blog"] = Blog::orderBy('sortOrder')->get();

        $data["pageTitle"] = 'blog';
        return view('frontened.blog')->with($data);
    }

    public function blogDetail($id)
    {
        $data["blog"] = Blog::where('id', $id)->orderBy('sortOrder')->firstOrFail();


        $data["pageTitle"] = 'blog';
        return view('frontened.blogDetail')->with($data);
    }

    /**
     * Update Status resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPriceByMeasurement(Request $request)
    {
        $width = $request->width;
        $height = $request->height;

        $squareMeter = $width * $height;

        $dimesionData = DB::select("SELECT MAX(price) as price FROM product_dimension where squareMetersMin <= $squareMeter LIMIT 1 ");

        // $result = 1;

        if ($dimesionData) {
            $response = array('status' => 1, 'message' => 'Status updated', 'response' => $dimesionData);
        } else {
            $response = array('status' => 0, 'message' => 'Something went wrong', 'response' => '');
        }

        return response()->json($response);
    }

    public function newsletterAdd(Request $request)
    {

        $email = $request->input('email');


        // Sending email

        $toEmail = $email;
        $subject = "Subject Goes here";
        $emailMessage = "Message Body Goes Here Newslettre";
        $fromEmail = "contact@bradley.com";
        $fromName = "Bradeley";

        Mail::send([], [], function ($message) use ($toEmail, $subject, $fromEmail, $fromName, $emailMessage) {
            $message->to($toEmail)->subject($subject);
            $message->from($fromEmail, $fromName);
            $message->setBody($emailMessage, 'text/html'); // for HTML rich messages

        });

        // Sending email

        return Redirect::to("newsletter/thanks");
    }

    public function newsletterThanks()
    {

        $data["pageTitle"] = 'blog';
        return view('frontened.newsletterThanks')->with($data);
    }


    public function checkout()
    {
        $data['cartData'] = Session::get('cartData');


        $data["pageTitle"] = 'Checkout';
        return view('frontened.checkout')->with($data);
    }

    public function checkoutAdd(Request $request)
    {

        $createAccount = $request->input('createAccount');
        $customerData = $request->session()->get('customerSession');
        $cartData = Session::get('cartData');

        // echo "<pre>"; print_r($cartData);die();

        $customerId = NULL;

        if (isset($customerData->id) && !empty($customerData->id)) {

            $customerId = $customerData->id;
        }

        // Addming Customer Information if Create Account Button Is Checked

        if ($createAccount == 1) {

            $customer = new Customer();

            $customer->firstName = $request->input('firstName');
            $customer->lastName = $request->input('lastName');
            $customer->email = $request->input('email');
            $customer->phone = $request->input('billingPhone');
            $customer->password = bcrypt($request->input('password'));

            $customer->status = 1;
            $customer->sortOrder = 1;
            $customer->increment('sortOrder');

            $customer->save();

            $customerId = $customer->id;
        }

        // Adding Order Information

        $order = new Order();
        $order->customerId = $customerId;
        $order->customerEmail = $request->input('email');
        $order->customerName = $request->input('firstName') . ' ' . $request->input('lastName');
        $order->totalAmount = $cartData['cartAmount'];
        $order->orderStatus = 1;

        $order->status = 1;
        $order->sortOrder = 1;
        $order->increment('sortOrder');

        $order->save();

        // Adding Billing Address

        $orderBilling = new OrderBilling();
        $orderBilling->orderId = $order->id;
        $orderBilling->address1 = $request->input('bllingAddress1');
        $orderBilling->address2 = $request->input('bllingAddress2');
        $orderBilling->country = $request->input('billingCountry');
        $orderBilling->zip = $request->input('billingZip');
        $orderBilling->state = $request->input('billingState');
        $orderBilling->company = $request->input('bllingCompany');

        $orderBilling->save();

        //Adding Order Items

        foreach ($cartData['cartItems'] as $value) {

            $orderItem = new OrderItem();
            $orderItem->orderId = $order->id;
            $orderItem->name = $value['name'];
            $orderItem->type = $value['type'];
            $orderItem->value = $value['id'];
            $orderItem->price = $value['price'];

            $orderItem->save();
        }




        return Redirect::to("proceedToPayment");
    }

    public function cartAdd(Request $request)
    {

        // session(['cartData' => $request->all()]);

        $formData = $request->all();

        // print_r($formData);die();

        $cartData = array();

        $cartData['cartAmount'] = $formData['cartAmount'];

        /////////////////

        if (!empty($formData['width']) && !empty($formData['height'])) {

            $cartData['cartItems'][0]['name'] = " (" . $formData['width'] . " * " . $formData['height'] . ")";
            $cartData['cartItems'][0]['type'] = "dimension";
            $cartData['cartItems'][0]['id'] = '';
            $cartData['cartItems'][0]['price'] = $formData['valueByDimension'];
        }

        //////////////

        if (!empty($formData['shutter-style-id'])) {

            $cartData['cartItems'][1]['name'] = $formData['shutter-style-name'];
            $cartData['cartItems'][1]['type'] = "shutter-style";
            $cartData['cartItems'][1]['id'] =  $formData['shutter-style-id'];
            $cartData['cartItems'][1]['price'] = '';
        }

        //////////////

        if (!empty($formData['shutter-type-id'])) {

            $cartData['cartItems'][2]['name'] = $formData['shutter-type-name'];
            $cartData['cartItems'][2]['type'] = "shutter-type";
            $cartData['cartItems'][2]['id'] =  $formData['shutter-type-id'];
            $cartData['cartItems'][2]['price'] = $formData['valueByType'];
        }

        //////////////

        if (!empty($formData['recess-id'])) {

            $cartData['cartItems'][3]['name'] = $formData['recess-name'];
            $cartData['cartItems'][3]['type'] = "recess";
            $cartData['cartItems'][3]['id'] =  $formData['recess-id'];
            $cartData['cartItems'][3]['price'] = '';
        }

        //////////////

        if (!empty($formData['frame-id'])) {

            $cartData['cartItems'][4]['name'] = $formData['frame-name'];
            $cartData['cartItems'][4]['type'] = "frame";
            $cartData['cartItems'][4]['id'] =  $formData['frame-id'];
            $cartData['cartItems'][4]['price'] = '';
        }

        //////////////

        if (!empty($formData['panel'])) {

            $cartData['cartItems'][5]['name'] = $formData['panel-name'];
            $cartData['cartItems'][5]['type'] = "panel";
            $cartData['cartItems'][5]['id'] =  $formData['panel'];
            $cartData['cartItems'][5]['price'] = '';
        }

        //////////////


        if (!empty($formData['louvre-id'])) {

            $cartData['cartItems'][6]['name'] = $formData['louvre-name'];
            $cartData['cartItems'][6]['type'] = "louvre";
            $cartData['cartItems'][6]['id'] =  $formData['louvre-id'];
            $cartData['cartItems'][6]['price'] = '';
        }

        //////////////

        if (!empty($formData['tiltrod-id'])) {

            $cartData['cartItems'][7]['name'] = $formData['tiltrod-name'];
            $cartData['cartItems'][7]['type'] = "tiltrod";
            $cartData['cartItems'][7]['id'] =  $formData['tiltrod-id'];
            $cartData['cartItems'][7]['price'] = $formData['valueByTiltrod'];
        }

        //////////////

        if (!empty($formData['hinge-id'])) {

            $cartData['cartItems'][8]['name'] = $formData['hinge-name'];
            $cartData['cartItems'][8]['type'] = "hinge";
            $cartData['cartItems'][8]['id'] =  $formData['hinge-id'];
            $cartData['cartItems'][8]['price'] = $formData['valueByHinge'];
        }

        //////////////

        // if(!empty($formData['hinge-id'])){

        //     $cartData['cartItems'][9]['name'] = $formData['hinge-name'];
        //     $cartData['cartItems'][9]['type'] = "hinge";
        //     $cartData['cartItems'][9]['id'] =  $formData['hinge-id'];

        // }

        //////////////

        session(['cartData' => $cartData]);

        return Redirect::to("checkout");
    }

    public function search(Request $request)
    {

        $formData = $request->all();
        $query = $formData['query'];

        $data["blog"] = Blog::where('title', 'like', '%' . $query  . '%')->orderBy('sortOrder')->get();

        //print_r( $data["blog"]);die();

        $data["query"] = $query;

        $data["pageTitle"] = 'blog';
        return view('frontened.blog')->with($data);
    }
    // public function price()
    // {

    //     $data["pageTitle"] = 'Applauce';
    //     return view('frontened.price')->with($data);
    // }

    // public function terms()
    // {

    //     $data["pageTitle"] = 'Applauce';
    //     return view('frontened.terms')->with($data);
    // }

    // public function subscribe()
    // {

    //     $data["pageTitle"] = 'Applauce';
    //     return view('frontened.subscribe')->with($data);
    // }

    // public function storeSubscription(Request $request)
    // {


    //     $rules = [
    //         'companyName' => 'required',
    //         'mailTemplate' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //         'companyLogo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //     ];

    //     $customMessages = [

    //         'companyName.required' => 'The company field can not be blank.',
    //     ];

    //     $this->validate($request, $rules, $customMessages);



    //     $employeeDataJSON = '';

    //     $employeeName = $request->input('employeeName');
    //     $employeePhone = $request->input('employeePhone');
    //     $employeeMail = $request->input('employeeMail');

    //     if (isset($employeeName) && !empty($employeeName)) {

    //         $employeeData = array();
    //         $i = 0;
    //         foreach ($employeeName as $value) {

    //             $employeeData[$i]['name'] = $employeeName[$i];
    //             $employeeData[$i]['phone'] = $employeePhone[$i];
    //             $employeeData[$i]['mail'] = $employeeMail[$i];

    //             $i++;
    //         }

    //         $employeeDataJSON = json_encode($employeeData);
    //     }

    //     $company = new Company();

    //     // Image Uploading

    //     if ($request->hasFile('mailTemplate')) {  // Check if file input is set

    //         $mediaId = imageUpload($request->mailTemplate, '', 0, "uploads/subscribe/"); //Image, ReferenceRecordId, UserId, Path

    //         $company->mailTemplateId = $mediaId;
    //     }


    //     if ($request->hasFile('companyLogo')) {  // Check if file input is set

    //         $mediaId = imageUpload($request->companyLogo, '', 0, "uploads/subscribe/"); //Image, ReferenceRecordId, UserId, Path

    //         $company->companyLogoId = $mediaId;
    //     }

    //     $company->companyName = $request->input('companyName');
    //     $company->address1 = $request->input('address1');
    //     $company->address2 = $request->input('address2');
    //     $company->zipCode = $request->input('zipCode');
    //     $company->town = $request->input('town');
    //     $company->country = $request->input('country');
    //     $company->phone = $request->input('phone');
    //     $company->mailAddress = $request->input('mailAddress');
    //     $company->webAddress = $request->input('webAddress');
    //     $company->sendSms = $request->input('sendSms');
    //     $company->employeeData = $employeeDataJSON;

    //     $company->status = 1;
    //     $company->sortOrder = 1;
    //     $company->increment('sortOrder');

    //     $company->save();

    //     $employeeHtml = '';
    //     $e=1;
    //     foreach($employeeData as $values){

    //         $employeeHtml.= "<p>".$e.". ".$values['name'].' | '.$values['phone'].' | '.$values['mail'].'</p>';

    //     $e++; }

    //     $sendSMS = '';

    //     if($company->sendSms == 1){ $sendSMS = "Yes"; }else{ $sendSMS = "NO";}


    //     $emailMessage = "<table>
    //     <tr><td>Company Name:</td><td>" . $company->companyName . "</td></tr>
    //     <tr><td>Address 1:</td><td>" . $company->address1 . "</td></tr>
    //     <tr><td>Address 2:</td><td>" . $company->address2 . "</td></tr>
    //     <tr><td>Zip Code:</td><td>" . $company->zipCode . "</td></tr>
    //     <tr><td>Town:</td><td>" . $company->town . "</td></tr>
    //     <tr><td>Country:</td><td>" . $company->country . "</td></tr>
    //     <tr><td>Phone:</td><td>" . $company->phone . "</td></tr>
    //     <tr><td>Mail Address:</td><td>" . $company->mailAddress . "</td></tr>
    //     <tr><td>Web Address:</td><td>" . $company->webAddress . "</td></tr>
    //     <tr><td>Do you want to have the option to send SMS?:</td><td>" . $sendSMS . "</td></tr>
    //     <tr><td>Employee Details:</td><td>" . $employeeHtml . "</td></tr>

    //     </table>";

    //     if (!empty($company->mailTemplateId)) {

    //         $pathToMailTemplate = public_path('/') . "/uploads/subscribe/" . $company->mailTemplate->name;
    //     } else {

    //         $pathToMailTemplate = '';
    //     }


    //     if (!empty($company->companyLogoId)) {

    //         $pathToLogo = public_path('/') . "/uploads/subscribe/" . $company->companyLogo->name;
    //     } else {

    //         $pathToLogo = '';
    //     }

    //     Mail::send([], [], function ($message) use ($emailMessage, $pathToMailTemplate, $pathToLogo) {
    //         $message->to("post@stenolav-management.no")->subject("Applauce Subscription Lead");
    //         $message->from("info@applauce.bay20.com", "Applauce");
    //         // here comes what you want
    //         ///->setBody('Hi, welcome user!'); // assuming text/plain
    //         // or:
    //         $message->setBody($emailMessage, 'text/html'); // for HTML rich messages

    //         if (!empty($pathToMailTemplate)) {
    //             $message->attach($pathToMailTemplate, array());
    //         }

    //         if (!empty($pathToLogo)) {
    //             $message->attach($pathToLogo, array());
    //         }
    //     });

    //     // Sending email

    //     return redirect()->route('subscribe')->with('message', 'Subscription Detials Send Successfully');
    // }
}
