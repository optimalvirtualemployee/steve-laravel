<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;


use Closure;

class Checkusertype
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $roleId = Auth::user()->roleId;

        // if($roleId == 3 || $roleId == 4){
        //     return redirect('/admin/login')->with('message', ' It looks like you are not allowed to access this page directly. Please contact your system
        //     administrator.');
        // }
       
        return $next($request);
    }
}
