<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;


use Closure;

class Parentcheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roleId = Auth::guard('front')->user()->roleId;

        if($roleId != 3){
            return redirect('/permission-denied');
        }
       
        return $next($request);
    }
}
