<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('roleId')->nullable();
            $table->unsignedBigInteger('parentId')->nullable();
            $table->string('username')->nullable();
            $table->string('name')->nullable();
            $table->string('imageId')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('meetingUsername')->nullable();
            $table->string('meetingPassword')->nullable();
            $table->tinyInteger('status');
            $table->integer('sortOrder');
            $table->timestamps();
            $table->foreign('roleId')->references('id')->on('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}