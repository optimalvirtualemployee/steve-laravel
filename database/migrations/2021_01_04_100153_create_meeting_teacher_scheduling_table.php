<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingTeacherSchedulingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_teacher_scheduling', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('meetingId')->nullable();
            $table->unsignedBigInteger('teacherId')->nullable();
            $table->dateTime('meetingDate')->nullable();
            $table->dateTime('meetingTime')->nullable();
            $table->tinyInteger('status');
            $table->integer('sortOrder');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_teacher_scheduling');
    }
}
