<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_order_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orderId')->nullable();
            $table->unsignedBigInteger('programId')->nullable();
            $table->unsignedBigInteger('subjectId')->nullable();
            $table->unsignedBigInteger('topicId')->nullable();
            $table->unsignedBigInteger('timeSlotId')->nullable();
            $table->unsignedBigInteger('stateId')->nullable();
            $table->integer('noOfSessionn')->nullable();
            $table->integer('amount')->nullable();
            $table->tinyInteger('status');
            $table->integer('sortOrder');
            $table->timestamps(); 
            $table->foreign('orderId')->references('id')->on('lms_order')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_order_item');
    }
}
