<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('userId')->nullable();
            $table->string('customerFirstName', 500)->nullable();
            $table->string('customerLastName', 500)->nullable();
            $table->string('customerEmail', 500)->nullable();
            $table->string('customerContact')->nullable();
            $table->string('billingState')->nullable();
            $table->string('billingCountry')->nullable();
            $table->string('billingPostCode')->nullable();
            $table->unsignedBigInteger('sessionRecieverId')->nullable();
            $table->integer('totalAmount')->nullable();
            $table->integer('discount')->nullable();
            $table->text('paymentGateway')->nullable();
            $table->tinyInteger('status');
            $table->integer('sortOrder');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_order');
    }
}
