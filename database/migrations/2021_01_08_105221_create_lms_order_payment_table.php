<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLmsOrderPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_order_payment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orderId')->nullable();
            $table->text('txnNumber')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('paymentStatus')->nullable();
            $table->tinyInteger('status');
            $table->integer('sortOrder');
            $table->timestamps();  
            $table->foreign('orderId')->references('id')->on('lms_order')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_order_payment');
    }
}
