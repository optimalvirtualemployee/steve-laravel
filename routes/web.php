<?php
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/email', function () {
//     Mail::to('ashish@optimalvirtualemployee.com')->send(new WelcomeMail());
//     return new WelcomeMail();
// });

//Auth::routes();

Route::get('/teacherEarlyMorningReminder', 'CronController@teacherEarlyMorningReminder');
Route::get('/cron/send-to-student-before-one-hour', 'CronController@sendToStudentBeforeOneHour');


Route::get('/login', 'HomeController@showFrontLoginForm')->name('user.login');
Route::post('/account/login', 'HomeController@frontLogin');


Route::get('/admin/login', 'admin\AdminController@login')->name('adminLogin');
Route::post('login', 'admin\AdminController@doLogin')->name('customeLogin');
Route::get('/admin/register', 'admin\AdminController@register');
Route::post('register', 'admin\AdminController@createUser');
Route::get('/', 'HomeController@index')->name('home');

//Route::get('/login', 'HomeController@login')->name('login');
Route::get('/register', 'HomeController@register')->name('register');
Route::get('/forgot-password', 'HomeController@forgotPassword')->name('forgot-password');
Route::get('/account', 'CustomerController@account')->name('account');
// Route::get('/order/{id}', 'CustomerController@orderDetail')->name('orderDetail');
Route::post('/account/register', 'HomeController@storeRegistration');
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::post('/contact', 'HomeController@contactUs');
Route::post('/newsLetter', 'frontend\ProgramController@newsLetter');   

//payment module
// Route::get('/payment', 'PaymentController@payment')->name('payment');

Route::get('/blog', 'HomeController@blog')->name('blog');
Route::get('/blog/{id}', 'HomeController@blogDetail')->name('blogDetail');
Route::post('/get-price-by-measurement', 'HomeController@getPriceByMeasurement');
Route::get('/admin/logout', 'admin\AdminController@logout');

//front program page
Route::get('/program/{id}', 'frontend\ProgramController@index')->name('programDetail');
Route::get('/program-version2/{id}', 'frontend\ProgramController@index1')->name('programDetail1');

//front end program page

// forntend account account dashboard routes

Route::group(['middleware' => ['auth:front']], function () {

Route::post('account-dashboard/{id}', 'frontend\AccountController@accountProfileUpdate');
Route::post('/user/updatePassword/{id}', [
        'as' => 'user.updatePassword',
        'uses' => 'frontend\AccountController@updatePassword'
  ]);
Route::get('/account/dashboard', 'frontend\AccountController@accountDashboard')->name('accountDashboard');
Route::get('/account/profile', 'frontend\AccountController@accountProfile')->name('accountProfile');
Route::get('/account/change-password', 'frontend\AccountController@changePassword')->name('accountChangePassword');
Route::get('/account/manage-session', 'frontend\AccountController@manageSession')->name('manageSession');
Route::get('account/edit-session/{id}/{userId}', ['as' => 'editSession', 'uses' => 'frontend\AccountController@editSession']);

Route::get('account/re-schedule-session/{id}/{userId}', ['as' => 'reScheduleSession', 'uses' => 'frontend\AccountController@reScheduleSession']);

Route::post('/account/meeting/update-session-missed/{id}', ['as' => 'meeting.updateMeetingMissed','uses' => 'frontend\AccountController@updateMeetingMissed']);

Route::post('/account/meeting/update/{id}', ['as' => 'meeting.updateMeeting','uses' => 'frontend\AccountController@updateMeeting']);

Route::get('account/order', 'frontend\AccountController@index');
Route::get('account/show/{id}', 'frontend\AccountController@show')->name('account.show');


//book session module
Route::get('/book-session', 'frontend\BookSessionController@index')->name('book-session');
Route::post('book-session/store/', ['as' => 'bookSession.store', 'uses' => 'frontend\BookSessionController@store']);
Route::post('frontend/subject/getBookSubjectByProgramId', 'frontend\BookSessionController@getBookSubjectByProgramId');
Route::post('frontend/topic/getBookTopicBySubjectId', 'frontend\BookSessionController@getBookTopicBySubjectId');
Route::post('frontend/topic/getPlanByTopicQty', 'frontend\BookSessionController@getPlanByTopicQty');
Route::post('frontend/topic/getPlanByTopicId', 'frontend\BookSessionController@getPlanByTopicId');

Route::get('/payment/{id}', 'frontend\BookSessionController@proceedPayment')->name('payment');
Route::post('frontend/paymentreq', 'frontend\BookSessionController@payment');

// webinar session
Route::get('meeting/webinar/{id}', 'frontend\MeetingController@index');

//end book session

Route::group(['middleware' => ['parentcheck']], function () {


Route::get('/account/manage-children', 'frontend\AccountController@manageChildren')->name('manageChildren');
Route::get('/account/add-children', 'frontend\AccountController@addChildren')->name('addChildren');
Route::get('account/{id}/edit', ['as' => 'account.edit', 'uses' => 'frontend\AccountController@edit']);
Route::put('account/{id}', ['as' => 'account.update', 'uses' => 'frontend\AccountController@update']);
Route::post('account/store', ['as' => 'account.store', 'uses' => 'frontend\AccountController@store']);

});

});

Route::get('/payment-successfull', 'frontend\PageController@paymentSuccess')->name('paymentSuccess');

Route::get('/{slug}', 'frontend\PageController@page')->name('pageDetail');

// admin routes
Route::group(['middleware' => ['auth']], function () {

    Route::get('/admin/permission-denied', 'admin\DashboardController@permissionDenied');

      

    //teacher Meeting Routings
     
     Route::post('admin/teacher-meeting/updateSortorder', 'admin\TeacherMeetingController@updateSortorder');
     Route::post('admin/teacher-meeting/destroyAll', 'admin\TeacherMeetingController@destroyAll');
     Route::post('admin/teacher-meeting/updateStatus', 'admin\TeacherMeetingController@updateStatus');
     Route::resource('admin/teacher-meeting', 'admin\TeacherMeetingController');

     Route::get('admin/teacher-meeting/activate/{id}', 'admin\TeacherMeetingController@meetingActivate');
     Route::post('/teacher-meeting/activate/update/{id}', ['as' => 'teacherMeeting.meetingActivateUpdate','uses' => 'admin\TeacherMeetingController@meetingActivateUpdate']);
 
    //teacher Meeting Routings ends

    //meeting techer scheduling Routings

    //  Route::post('admin/meeting-teacher-scheduling/updateSortorder', 'admin\MeetingTeacherSchedulingController@updateSortorder');
    //  Route::post('admin/meeting-teacher-scheduling/destroyAll', 'admin\MeetingTeacherSchedulingController@destroyAll');
    //  Route::post('admin/meeting-teacher-scheduling/updateStatus', 'admin\MeetingTeacherSchedulingController@updateStatus');
    //  Route::resource('admin/meeting-teacher-scheduling', 'admin\MeetingTeacherSchedulingController');
 
     //meeting techer scheduling Routings ends

    //teacher class Routings

    // Route::post('admin/teacher-class/updateSortorder', 'admin\TeacherClassController@updateSortorder');
    // Route::post('admin/teacher-class/destroyAll', 'admin\TeacherClassController@destroyAll');
    // Route::post('admin/teacher-class/updateStatus', 'admin\TeacherClassController@updateStatus');
    // Route::resource('admin/teacher-class', 'admin\TeacherClassController');
    // Route::post('admin/studentList', 'admin\TeacherClassController@studentList');
 
     //teacher class Routings ends

     //Blog Routings

      
       
         Route::get('admin/user/{id}/edit', ['as' => 'user.edit', 'uses' => 'admin\UserController@edit']);
        
         Route::put('admin/user/{id}', ['as' => 'user.update', 'uses' => 'admin\UserController@update']);


     
        //Meeting Routings ends

       Route::get('/admin/dashboard', 'admin\DashboardController@home');

       Route::group(['middleware' => ['admin']], function () {

           //Meeting Routings

     Route::post('admin/topic/getTopicBySubjectId', 'admin\MeetingController@getTopicBySubjectId');
     Route::post('admin/topic/getTeacherByProgramIDSubjectId', 'admin\MeetingController@getTeacherByProgramIDSubjectId');
     Route::post('admin/meeting/updateSortorder', 'admin\MeetingController@updateSortorder');
     Route::post('admin/meeting/destroyAll', 'admin\MeetingController@destroyAll');
     Route::post('admin/meeting/updateStatus', 'admin\MeetingController@updateStatus');
     Route::get('admin/meeting/activate/{id}', 'admin\MeetingController@meetingActivate');
     Route::post('/meeting/activate/update/{id}', ['as' => 'meeting.meetingActivateUpdate','uses' => 'admin\MeetingController@meetingActivateUpdate']);
     Route::resource('admin/meeting', 'admin\MeetingController');
 
    //Meeting Routings ends

         //Question Catgory Routings
    
       Route::post('admin/question-category/updateSortorder', 'admin\QuestionCategoryController@updateSortorder');
       Route::post('admin/question-category/destroyAll', 'admin\QuestionCategoryController@destroyAll');
       Route::post('admin/question-category/updateStatus', 'admin\QuestionCategoryController@updateStatus');
       Route::resource('admin/question-category', 'admin\QuestionCategoryController');
   
       //QUestion Category Routings ends

       //Question Routings

       Route::post('admin/question/updateSortorder', 'admin\QuestionController@updateSortorder');
       Route::post('admin/question/destroyAll', 'admin\QuestionController@destroyAll');
       Route::post('admin/question/updateStatus', 'admin\QuestionController@updateStatus');
       Route::resource('admin/question', 'admin\QuestionController');
   
       //QUestion Routings ends

       

        Route::get('admin/plan/topic/{programId}/{subjectId}', ['as' => 'plan.topic', 'uses' => 'admin\PlanController@topic']);
    
        Route::post('admin/plan/delete/{id}', ['as' => 'plan.delete', 'uses' => 'admin\PlanController@delete']);
    
        Route::resource('admin/plan', 'admin\PlanController');
    
       //Testimonial Routings
    
       Route::post('admin/testimonial/updateSortorder', 'admin\TestimonialController@updateSortorder');
       Route::post('admin/testimonial/destroyAll', 'admin\TestimonialController@destroyAll');
       Route::post('admin/testimonial/updateStatus', 'admin\TestimonialController@updateStatus');
       Route::resource('admin/testimonial', 'admin\TestimonialController');
    
       //Testimonial Routings ends
    
    
        //Program Routings
    
        Route::post('admin/program/updateSortorder', 'admin\ProgramController@updateSortorder');
        Route::post('admin/program/destroyAll', 'admin\ProgramController@destroyAll');
        Route::post('admin/program/updateStatus', 'admin\ProgramController@updateStatus');
        Route::resource('admin/program', 'admin\ProgramController');
    
        //Program Routings ends
    
        //Page Routings
    
        Route::post('admin/page/updateSortorder', 'admin\PageController@updateSortorder');
        Route::post('admin/page/destroyAll', 'admin\PageController@destroyAll');
        Route::post('admin/page/updateStatus', 'admin\PageController@updateStatus');
        Route::resource('admin/page', 'admin\PageController');
    
        //Page Routings ends
    
          //Subject Routings
          Route::post('admin/subject/getSubjectByProgramId', 'admin\SubjectController@getSubjectByProgramId');
          Route::post('admin/subject/updateSortorder', 'admin\SubjectController@updateSortorder');
          Route::post('admin/subject/destroyAll', 'admin\SubjectController@destroyAll');
          Route::post('admin/subject/updateStatus', 'admin\SubjectController@updateStatus');
          Route::resource('admin/subject', 'admin\SubjectController');
      
          //Program Routings ends
    
        //Subscription Routings
    
         Route::post('admin/subscription/updateSortorder', 'admin\SubscriptionController@updateSortorder');
         Route::post('admin/subscription/destroyAll', 'admin\SubscriptionController@destroyAll');
         Route::post('admin/subscription/updateStatus', 'admin\SubscriptionController@updateStatus');
         Route::resource('admin/subscription', 'admin\SubscriptionController');
     
        //Subscription Routings ends
    
        //Subscription Member Routings
    
         Route::post('admin/subscription-member/updateSortorder', 'admin\SubscriptionMemberController@updateSortorder');
         Route::post('admin/subscription-member/destroyAll', 'admin\SubscriptionMemberController@destroyAll');
         Route::post('admin/subscription-member/updateStatus', 'admin\SubscriptionMemberController@updateStatus');
         Route::resource('admin/subscription-member', 'admin\SubscriptionMemberController');
     
         //Subscription Member Routings ends
    
           
    
        //meeting techer scheduling Routings
    
        //  Route::post('admin/meeting-teacher-scheduling/updateSortorder', 'admin\MeetingTeacherSchedulingController@updateSortorder');
        //  Route::post('admin/meeting-teacher-scheduling/destroyAll', 'admin\MeetingTeacherSchedulingController@destroyAll');
        //  Route::post('admin/meeting-teacher-scheduling/updateStatus', 'admin\MeetingTeacherSchedulingController@updateStatus');
        //  Route::resource('admin/meeting-teacher-scheduling', 'admin\MeetingTeacherSchedulingController');
     
         //meeting techer scheduling Routings ends
    
        //teacher class Routings
    
        Route::post('admin/teacher-class/updateSortorder', 'admin\TeacherClassController@updateSortorder');
        Route::post('admin/teacher-class/destroyAll', 'admin\TeacherClassController@destroyAll');
        Route::post('admin/teacher-class/updateStatus', 'admin\TeacherClassController@updateStatus');
        Route::resource('admin/teacher-class', 'admin\TeacherClassController');
        Route::post('admin/studentList', 'admin\TeacherClassController@studentList');
     
         //teacher class Routings ends
    
         //Blog Routings
    
         Route::post('admin/blog/updateSortorder', 'admin\BlogController@updateSortorder');
         Route::post('admin/blog/destroyAll', 'admin\BlogController@destroyAll');
         Route::post('admin/blog/updateStatus', 'admin\BlogController@updateStatus');
         Route::resource('admin/blog', 'admin\BlogController');
     
         //Blog Routings ends
    
        //Banner Routings
    
         Route::post('admin/banner/updateSortorder', 'admin\BannerController@updateSortorder');
         Route::post('admin/banner/destroyAll', 'admin\BannerController@destroyAll');
         Route::post('admin/banner/updateStatus', 'admin\BannerController@updateStatus');
         Route::resource('admin/banner', 'admin\BannerController');
     
         //Banner Routings ends
    
    
         //Contact Routings
    
         Route::post('admin/contact/updateSortorder', 'admin\ContactController@updateSortorder');
         Route::post('admin/contact/destroyAll', 'admin\ContactController@destroyAll');
         Route::post('admin/contact/updateStatus', 'admin\ContactController@updateStatus');
         Route::resource('admin/contact', 'admin\ContactController');
     
         //Contact Routings ends
    
         //Blog Routings
    
         Route::post('admin/user/updateSortorder', 'admin\UserController@updateSortorder');
         Route::post('admin/user/destroyAll', 'admin\UserController@destroyAll');
         Route::post('admin/user/updateStatus', 'admin\UserController@updateStatus');
         Route::resource('admin/user', 'admin\UserController');
     
         //Blog Routings ends
    
        //Master/States Routings
    
         Route::post('admin/state/updateSortorder', 'admin\StatesController@updateSortorder');
         Route::post('admin/state/destroyAll', 'admin\StatesController@destroyAll');
         Route::post('admin/state/updateStatus', 'admin\StatesController@updateStatus');
         Route::resource('admin/state', 'admin\StatesController');
     
         //Master/States Routings ends
    
        //Master/Time-Slot Routings
    
         Route::post('admin/time-slot/updateSortorder', 'admin\TimeSlotController@updateSortorder');
         Route::post('admin/time-slot/destroyAll', 'admin\TimeSlotController@destroyAll');
         Route::post('admin/time-slot/updateStatus', 'admin\TimeSlotController@updateStatus');
         Route::resource('admin/time-slot', 'admin\TimeSlotController');
     
         //Master/Time-Slot  Routings ends
    
        //Topic Routings
         
         Route::post('admin/topic/updateSortorder', 'admin\TopicController@updateSortorder');
         Route::post('admin/topic/destroyAll', 'admin\TopicController@destroyAll');
         Route::post('admin/topic/updateStatus', 'admin\TopicController@updateStatus');
         Route::resource('admin/topic', 'admin\TopicController');
     
         //Topic Routings ends
    
           //Order Routings
    
        Route::post('admin/order/updateSortorder', 'admin\OrderController@updateSortorder');
        Route::post('admin/order/destroyAll', 'admin\OrderController@destroyAll');
        Route::post('admin/order/updateStatus', 'admin\OrderController@updateStatus');
        Route::resource('admin/order', 'admin\OrderController');
    
        //Order Routings endss





    });    

    /*

    //-------Console Product Routing---------//

    //Route for passing multiple parameters
    //View Syntax {route('prom.edit',['id' => $value->id,'catId' => 1])}}
    //------
    //Route::get('admin/promoter/edit/{id}/{catId}', ['as' => 'prom.edit', 'uses' => 'admin\PromoterController@edit']);
    Route::post('admin/product/updateSortorder', 'admin\ProductController@updateSortorder');
    Route::post('admin/product/destroyAll', 'admin\ProductController@destroyAll');
    Route::post('admin/product/updateStatus', 'admin\ProductController@updateStatus');
    Route::get('/admin/product/dimension/{productId}', 'admin\ProductController@dimension');
    Route::resource('admin/product', 'admin\ProductController');

    //Product Dimension Routings

    Route::get('admin/product-dimension/{productId}/', ['as' => 'productDimension.index', 'uses' => 'admin\ProductDimensionController@index']);
    Route::get('admin/product-dimension/create/{productId}/', ['as' => 'productDimension.create', 'uses' => 'admin\ProductDimensionController@create']);
    Route::get('admin/product-dimension/edit/{id}/{productId}/', ['as' => 'productDimension.edit', 'uses' => 'admin\ProductDimensionController@edit']);
    Route::post('admin/product-dimension/store/', ['as' => 'productDimension.store', 'uses' => 'admin\ProductDimensionController@store']);
    Route::put('admin/product-dimension/update/', ['as' => 'productDimension.update', 'uses' => 'admin\ProductDimensionController@update']);
    Route::delete('admin/product-dimension/destroy/', ['as' => 'productDimension.delete', 'uses' => 'admin\ProductDimensionController@destroy']);
    Route::post('admin/product-dimension/destroyAll', 'admin\ProductDimensionController@destroyAll');
    Route::post('admin/product-dimension/updateStatus', 'admin\ProductDimensionController@updateStatus');
    Route::post('admin/product-dimension/updateSortorder', 'admin\ProductDimensionController@updateSortorder');


    //Product Dimension Routings ends


    //Shutter Type Routings

    Route::get('admin/shutter-type/{productId}/', ['as' => 'shutterType.index', 'uses' => 'admin\ShutterTypeController@index']);
    Route::get('admin/shutter-type/create/{productId}/', ['as' => 'shutterType.create', 'uses' => 'admin\ShutterTypeController@create']);
    Route::get('admin/shutter-type/edit/{id}/{productId}/', ['as' => 'shutterType.edit', 'uses' => 'admin\ShutterTypeController@edit']);
    Route::post('admin/shutter-type/store/', ['as' => 'shutterType.store', 'uses' => 'admin\ShutterTypeController@store']);
    Route::put('admin/shutter-type/update/', ['as' => 'shutterType.update', 'uses' => 'admin\ShutterTypeController@update']);
    Route::delete('admin/shutter-type/destroy/', ['as' => 'shutterType.delete', 'uses' => 'admin\ShutterTypeController@destroy']);
    Route::post('admin/shutter-type/destroyAll', 'admin\ShutterTypeController@destroyAll');
    Route::post('admin/shutter-type/updateStatus', 'admin\ShutterTypeController@updateStatus');
    Route::post('admin/shutter-type/updateSortorder', 'admin\ShutterTypeController@updateSortorder');


    //Shutter Type Routings ends

    //Shutter Style Routings

    Route::get('admin/shutter-style/{productId}/', ['as' => 'shutterStyle.index', 'uses' => 'admin\ShutterStyleController@index']);
    Route::get('admin/shutter-style/create/{productId}/', ['as' => 'shutterStyle.create', 'uses' => 'admin\ShutterStyleController@create']);
    Route::get('admin/shutter-style/edit/{id}/{productId}/', ['as' => 'shutterStyle.edit', 'uses' => 'admin\ShutterStyleController@edit']);
    Route::post('admin/shutter-style/store/', ['as' => 'shutterStyle.store', 'uses' => 'admin\ShutterStyleController@store']);
    Route::put('admin/shutter-style/update/', ['as' => 'shutterStyle.update', 'uses' => 'admin\ShutterStyleController@update']);
    Route::delete('admin/shutter-style/destroy/', ['as' => 'shutterStyle.delete', 'uses' => 'admin\ShutterStyleController@destroy']);
    Route::post('admin/shutter-style/destroyAll', 'admin\ShutterStyleController@destroyAll');
    Route::post('admin/shutter-style/updateStatus', 'admin\ShutterStyleController@updateStatus');
    Route::post('admin/shutter-style/updateSortorder', 'admin\ShutterStyleController@updateSortorder');


    //Shutter Style Routings ends

    //Color Routings

    Route::get('admin/color/{productId}/', ['as' => 'color.index', 'uses' => 'admin\ColorController@index']);
    Route::get('admin/color/create/{productId}/', ['as' => 'color.create', 'uses' => 'admin\ColorController@create']);
    Route::get('admin/color/edit/{id}/{productId}/', ['as' => 'color.edit', 'uses' => 'admin\ColorController@edit']);
    Route::post('admin/color/store/', ['as' => 'color.store', 'uses' => 'admin\ColorController@store']);
    Route::put('admin/color/update/', ['as' => 'color.update', 'uses' => 'admin\ColorController@update']);
    Route::delete('admin/color/destroy/', ['as' => 'color.delete', 'uses' => 'admin\ColorController@destroy']);
    Route::post('admin/color/destroyAll', 'admin\ColorController@destroyAll');
    Route::post('admin/color/updateStatus', 'admin\ColorController@updateStatus');
    Route::post('admin/color/updateSortorder', 'admin\ColorController@updateSortorder');

    //Color Routings ends

    //Frame Routings

    Route::get('admin/frame/{productId}/', ['as' => 'frame.index', 'uses' => 'admin\FrameController@index']);
    Route::get('admin/frame/create/{productId}/', ['as' => 'frame.create', 'uses' => 'admin\FrameController@create']);
    Route::get('admin/frame/edit/{id}/{productId}/', ['as' => 'frame.edit', 'uses' => 'admin\FrameController@edit']);
    Route::post('admin/frame/store/', ['as' => 'frame.store', 'uses' => 'admin\FrameController@store']);
    Route::put('admin/frame/update/', ['as' => 'frame.update', 'uses' => 'admin\FrameController@update']);
    Route::delete('admin/frame/destroy/', ['as' => 'frame.delete', 'uses' => 'admin\FrameController@destroy']);
    Route::post('admin/frame/destroyAll', 'admin\FrameController@destroyAll');
    Route::post('admin/frame/updateStatus', 'admin\FrameController@updateStatus');
    Route::post('admin/frame/updateSortorder', 'admin\FrameController@updateSortorder');


    //Frame Routings ends

    //Recess Routings

    Route::get('admin/recess/{productId}/', ['as' => 'recess.index', 'uses' => 'admin\RecessController@index']);
    Route::get('admin/recess/create/{productId}/', ['as' => 'recess.create', 'uses' => 'admin\RecessController@create']);
    Route::get('admin/recess/edit/{id}/{productId}/', ['as' => 'recess.edit', 'uses' => 'admin\RecessController@edit']);
    Route::post('admin/recess/store/', ['as' => 'recess.store', 'uses' => 'admin\RecessController@store']);
    Route::put('admin/recess/update/', ['as' => 'recess.update', 'uses' => 'admin\RecessController@update']);
    Route::delete('admin/recess/destroy/', ['as' => 'recess.delete', 'uses' => 'admin\RecessController@destroy']);
    Route::post('admin/recess/destroyAll', 'admin\RecessController@destroyAll');
    Route::post('admin/recess/updateStatus', 'admin\RecessController@updateStatus');
    Route::post('admin/recess/updateSortorder', 'admin\RecessController@updateSortorder');


    //Recess Routings ends

    //Pannel Routings

    Route::get('admin/pannel/{productId}/', ['as' => 'pannel.index', 'uses' => 'admin\PannelController@index']);
    Route::get('admin/pannel/create/{productId}/', ['as' => 'pannel.create', 'uses' => 'admin\PannelController@create']);
    Route::get('admin/pannel/edit/{id}/{productId}/', ['as' => 'pannel.edit', 'uses' => 'admin\PannelController@edit']);
    Route::post('admin/pannel/store/', ['as' => 'pannel.store', 'uses' => 'admin\PannelController@store']);
    Route::put('admin/pannel/update/', ['as' => 'pannel.update', 'uses' => 'admin\PannelController@update']);
    Route::delete('admin/pannel/destroy/', ['as' => 'pannel.delete', 'uses' => 'admin\PannelController@destroy']);
    Route::post('admin/pannel/destroyAll', 'admin\PannelController@destroyAll');
    Route::post('admin/pannel/updateStatus', 'admin\PannelController@updateStatus');
    Route::post('admin/pannel/updateSortorder', 'admin\PannelController@updateSortorder');


    //Pannel Routings ends


    //Louvre Routings

    Route::get('admin/louvre/{productId}/', ['as' => 'louvre.index', 'uses' => 'admin\LouvreController@index']);
    Route::get('admin/louvre/create/{productId}/', ['as' => 'louvre.create', 'uses' => 'admin\LouvreController@create']);
    Route::get('admin/louvre/edit/{id}/{productId}/', ['as' => 'louvre.edit', 'uses' => 'admin\LouvreController@edit']);
    Route::post('admin/louvre/store/', ['as' => 'louvre.store', 'uses' => 'admin\LouvreController@store']);
    Route::put('admin/louvre/update/', ['as' => 'louvre.update', 'uses' => 'admin\LouvreController@update']);
    Route::delete('admin/louvre/destroy/', ['as' => 'louvre.delete', 'uses' => 'admin\LouvreController@destroy']);
    Route::post('admin/louvre/destroyAll', 'admin\LouvreController@destroyAll');
    Route::post('admin/louvre/updateStatus', 'admin\LouvreController@updateStatus');
    Route::post('admin/louvre/updateSortorder', 'admin\LouvreController@updateSortorder');


    //Louvre Routings ends

    //Tiltrod Routings

    Route::get('admin/tiltrod/{productId}/', ['as' => 'tiltrod.index', 'uses' => 'admin\TiltrodController@index']);
    Route::get('admin/tiltrod/create/{productId}/', ['as' => 'tiltrod.create', 'uses' => 'admin\TiltrodController@create']);
    Route::get('admin/tiltrod/edit/{id}/{productId}/', ['as' => 'tiltrod.edit', 'uses' => 'admin\TiltrodController@edit']);
    Route::post('admin/tiltrod/store/', ['as' => 'tiltrod.store', 'uses' => 'admin\TiltrodController@store']);
    Route::put('admin/tiltrod/update/', ['as' => 'tiltrod.update', 'uses' => 'admin\TiltrodController@update']);
    Route::delete('admin/tiltrod/destroy/', ['as' => 'tiltrod.delete', 'uses' => 'admin\TiltrodController@destroy']);
    Route::post('admin/tiltrod/destroyAll', 'admin\TiltrodController@destroyAll');
    Route::post('admin/tiltrod/updateStatus', 'admin\TiltrodController@updateStatus');
    Route::post('admin/tiltrod/updateSortorder', 'admin\TiltrodController@updateSortorder');


    //Tiltrod Routings ends


    //Hinge Routings

    Route::get('admin/hinge/{productId}/', ['as' => 'hinge.index', 'uses' => 'admin\HingeController@index']);
    Route::get('admin/hinge/create/{productId}/', ['as' => 'hinge.create', 'uses' => 'admin\HingeController@create']);
    Route::get('admin/hinge/edit/{id}/{productId}/', ['as' => 'hinge.edit', 'uses' => 'admin\HingeController@edit']);
    Route::post('admin/hinge/store/', ['as' => 'hinge.store', 'uses' => 'admin\HingeController@store']);
    Route::put('admin/hinge/update/', ['as' => 'hinge.update', 'uses' => 'admin\HingeController@update']);
    Route::delete('admin/hinge/destroy/', ['as' => 'hinge.delete', 'uses' => 'admin\HingeController@destroy']);
    Route::post('admin/hinge/destroyAll', 'admin\HingeController@destroyAll');
    Route::post('admin/hinge/updateStatus', 'admin\HingeController@updateStatus');
    Route::post('admin/hinge/updateSortorder', 'admin\HingeController@updateSortorder');

    //Hinge Routings ends


    //Blog Routings

    Route::post('admin/blog/updateSortorder', 'admin\BlogController@updateSortorder');
    Route::post('admin/blog/destroyAll', 'admin\BlogController@destroyAll');
    Route::post('admin/blog/updateStatus', 'admin\BlogController@updateStatus');
    Route::resource('admin/blog', 'admin\BlogController');

    //Blog Routings ends


    //Customer Routings

    Route::post('admin/customer/updateSortorder', 'admin\CustomerController@updateSortorder');
    Route::post('admin/customer/destroyAll', 'admin\CustomerController@destroyAll');
    Route::post('admin/customer/updateStatus', 'admin\CustomerController@updateStatus');
    Route::resource('admin/customer', 'admin\CustomerController');

    //Customer Routings ends

    //Order Routings

    Route::post('admin/order/updateSortorder', 'admin\OrderController@updateSortorder');
    Route::post('admin/order/destroyAll', 'admin\OrderController@destroyAll');
    Route::post('admin/order/updateStatus', 'admin\OrderController@updateStatus');
    Route::resource('admin/order', 'admin\OrderController');

    //Order Routings ends


    //Route for passing multiple parameters
    //View Syntax {route('prom.edit',['id' => $value->id,'catId' => 1])}}
    //------
    //Route::get('admin/promoter/edit/{id}/{catId}', ['as' => 'prom.edit', 'uses' => 'admin\PromoterController@edit']);
    Route::post('admin/promoter/updateSortorder', 'admin\PromoterController@updateSortorder');
    Route::post('admin/promoter/destroyAll', 'admin\PromoterController@destroyAll');
    Route::post('admin/promoter/updateStatus', 'admin\PromoterController@updateStatus');
    Route::resource('admin/promoter', 'admin\PromoterController');

    //-------Console Promoter Routing---------//

    //-------Console Country Routing---------//

    Route::post('admin/country/updateSortorder', 'admin\CountryController@updateSortorder');
    Route::post('admin/country/destroyAll', 'admin\CountryController@destroyAll');
    Route::post('admin/country/updateStatus', 'admin\CountryController@updateStatus');
    Route::resource('admin/country', 'admin\CountryController');

    //-------Console Country Routing---------//

    //-------Console Agency Routing---------//

    Route::post('admin/agency/updateSortorder', 'admin\AgencyController@updateSortorder');
    Route::post('admin/agency/destroyAll', 'admin\AgencyController@destroyAll');
    Route::post('admin/agency/updateStatus', 'admin\AgencyController@updateStatus');
    Route::resource('admin/agency', 'admin\AgencyController');

    //-------Console Agency Routing---------//

    //-------Console Agent Routing---------//

    Route::post('admin/agent/updateSortorder', 'admin\AgentController@updateSortorder');
    Route::post('admin/agent/destroyAll', 'admin\AgentController@destroyAll');
    Route::post('admin/agent/updateStatus', 'admin\AgentController@updateStatus');
    Route::resource('admin/agent', 'admin\AgentController');

    //-------Console Agent Routing---------//

    //-------Console Artist Routing---------//

    Route::post('admin/artist/getAgentByAgnecyId', 'admin\ArtistController@getAgentByAgnecyId');
    Route::post('admin/artist/updateSortorder', 'admin\ArtistController@updateSortorder');
    Route::post('admin/artist/destroyAll', 'admin\ArtistController@destroyAll');
    Route::post('admin/artist/updateStatus', 'admin\ArtistController@updateStatus');
    Route::resource('admin/artist', 'admin\ArtistController');

    //-------Console Artist Routing---------//

    //-------Console Contract Routing---------//

    Route::post('admin/contract/updateSortorder', 'admin\ContractController@updateSortorder');
    Route::post('admin/contract/destroyAll', 'admin\ContractController@destroyAll');
    Route::post('admin/contract/updateStatus', 'admin\ContractController@updateStatus');
    Route::resource('admin/contract', 'admin\ContractController');

    //-------Console Contract Routing---------//

    //-------Console Application Number Routing---------//

    Route::post('admin/application-number/updateSortorder', 'admin\ApplicationNumberController@updateSortorder');
    Route::post('admin/application-number/destroyAll', 'admin\ApplicationNumberController@destroyAll');
    Route::post('admin/application-number/updateStatus', 'admin\ApplicationNumberController@updateStatus');
    Route::resource('admin/application-number', 'admin\ApplicationNumberController');

    //-------Console Application Number Routing---------//
    */

     Route::fallback(function(){

    return View::make('errors.404');
        // return response()->json([
        //     'message' => 'Page Not Found. If error persists, contact info@steve.com'], 404);
    });
  
});
