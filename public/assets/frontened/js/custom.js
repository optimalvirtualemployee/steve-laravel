$(document).ready(function () {


  $(".shutter-type").click(function () {

    var priceByType = $('#valueByType').val();

    if (priceByType > 1) {

      $('#cartAmount').val(function (i, oldval) {
        return parseFloat(oldval) - parseFloat(priceByType);
      });

    }

    var price = $(this).attr("data-price");
    $('#valueByType').val(price);

    $('#cartAmount').val(function (i, oldval) {
      return parseFloat(oldval) + parseFloat(price);
    });

    var cartAmount = $('#cartAmount').val();
    $("#totalPrice").html(cartAmount);

  });


  $(".tiltrod").click(function () {

    var priceByTitlrod = $('#valueByTiltrod').val();

    if (priceByTitlrod > 1) {

      $('#cartAmount').val(function (i, oldval) {
        return parseFloat(oldval) - parseFloat(priceByTitlrod);
      });

    }

    var price = $(this).attr("data-price");

    var cartAmount = $('#cartAmount').val();

    var percentPrice = (price / 100) * cartAmount;

    $('#valueByTiltrod').val(percentPrice);

    $('#cartAmount').val(function (i, oldval) {
      return parseFloat(oldval) + parseFloat(percentPrice);
    });

    var amount = $('#cartAmount').val();


    $("#totalPrice").html(amount);

  });


  $(".hinge").click(function () {

    var priceByHinge = $('#valueByHinge').val();

    if (priceByHinge > 1) {

      $('#cartAmount').val(function (i, oldval) {
        return parseFloat(oldval) - parseFloat(priceByHinge);
      });

    }

    var price = $(this).attr("data-price");

    var cartAmount = $('#cartAmount').val();

    var percentPrice = (price / 100) * cartAmount;

    $('#valueByHinge').val(percentPrice);

    $('#cartAmount').val(function (i, oldval) {
      return parseFloat(oldval) + parseFloat(percentPrice);
    });

    var amount = $('#cartAmount').val();


    $("#totalPrice").html(amount);

  });


  $('.measureControl').change(function () {

    var dPrice = $('#valueByDimension').val();


    if (!$("#width").val() && !$("#height").val()) {

      $('#cartAmount').val(function (i, oldval) {
        return parseFloat(oldval) - parseFloat(dPrice);
      });

    }

    if (dPrice > 1) {

      $('#cartAmount').val(function (i, oldval) {
        return parseFloat(oldval) - parseFloat(dPrice);
      });

    }

    var width = $('#width').val();
    var height = $('#height').val();

    // var squareSpace = parseFloat(width) * parseFloat(height);

    ///alert(squareSpace);

    // if(squareSpace == NaN){

    //   $('#cartAmount').val( function(i, oldval) {
    //     return parseFloat(oldval) - parseFloat(dPrice);
    //   });

    // }


    $.ajax({
      type: "POST",
      data: {
        width: width,
        height: height,
      },
      url: '/get-price-by-measurement',
      dataType: 'json',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (result) {

        //console.log(result);

        $('#valueByDimension').val(result.response[0].price);

        $('#cartAmount').val(function (i, oldval) {
          return parseFloat(oldval) + parseFloat(result.response[0].price);
        });

        var cartAmount = $('#cartAmount').val();

        $("#totalPrice").html(cartAmount);
        //console.log(result.response[0].price);

      }
    });
  });



  /////////////////////////////////////////////////////////

  $(".toggle").click(function () {
    $(".menubar").toggle();
  });

  $(".tp-style").click(function () {
    $(this).next().toggle();
  });

  $(".search-icon").click(function () {
    $(".serch-section input").toggle();
  });



});

var jq = $;
const fieldsSteps = $('.fields');
const nextBtn = $('.next-prev-step a.next');
const prevBtn = $('.next-prev-step a.prev');
const stepDot = $('#steps a');

function stepOneValidation() { }
function stepTwoValidation() { }
function stepThreeValidation() { }
function stepFourValidation() { }
function stepFiveValidation() { }

function showNextStep() {
  nextBtn.click(function (e) {
    e.preventDefault();
    var getNextActiveId = $('.fields.active').next().attr('id');
    var getPrevActiveId = $('.fields.active').attr('id');
    $(this).attr('data-id', getNextActiveId);
    $(prevBtn).attr('data-id', getPrevActiveId);
    $('.fields.active').removeClass('active');

    /* show next step */
    $(`#${getNextActiveId}`).prev().hide();
    $(`#${getNextActiveId}`).prev().addClass('complete');
    $(`#${getNextActiveId}`).addClass('active');
    $(`#${getNextActiveId}`).show();

    /* add active and complete in steps */
    $(stepDot).removeClass('active');
    $(`a[href="#${getNextActiveId}"]`).addClass('active');
    $(`a[href="#${getPrevActiveId}"]`).addClass('complete');

    /* hide next button if no content */
    if ($(`#${getNextActiveId}`).next().length) {
      $(nextBtn).show();
    } else {
      $(nextBtn).hide();
    }
    prevBtn.show();
  });
}
showNextStep();

function showPrevStep() {
  prevBtn.click(function (e) {
    e.preventDefault();
    var getPrevActiveDiv = $(this).attr('data-id');

    /* deactivate current step */
    $(`#${getPrevActiveDiv}`).next().hide();
    $(`#${getPrevActiveDiv}`).next().removeClass('active');

    // activate current step
    $(`#${getPrevActiveDiv}`).show();
    $(`#${getPrevActiveDiv}`).addClass('active');
    $(`#${getPrevActiveDiv}`).removeClass('complete');

    // activate and deactivate dots
    $(stepDot).removeClass('active');
    $(`a[href="#${getPrevActiveDiv}"]`).addClass('active');
    $(`a[href="#${getPrevActiveDiv}"]`).removeClass('complete');

    // update next and prev data id
    var getPrevActiveId = $('.fields.active').prev().attr('id');
    var getNextActiveId = $('.fields.active').attr('id');
    $(nextBtn).attr('data-id', getNextActiveId);
    $(prevBtn).attr('data-id', getPrevActiveId);

    nextBtn.show();
    /* hide prev button if no content */
    if ($(`#${getPrevActiveDiv}`).prev().length) {
      $(prevBtn).show();
    } else {
      $(prevBtn).hide();
    }

  });
}

showPrevStep();
