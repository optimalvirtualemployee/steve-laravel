$(document).ready(function () {
    $("#programId").on("change", function () {
        var programId = this.value;

        $.ajax({
            type: "POST",
            data: {
                programId: programId,
            },
            url: "/frontend/subject/getBookSubjectByProgramId",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                $(".loading").show();
            },
            complete: function () {
                $(".loading").hide();
            },
            success: function (result) {
                $("#subjectId").html(result.response);
                $("#subjectId").prop("disabled", false);
                console.log(result);
            },
        });
    });

    $("#subjectId").on("change", function () {
        var subjectId = this.value;

        $.ajax({
            type: "POST",
            data: {
                subjectId: subjectId,
            },
            url: "/frontend/topic/getBookTopicBySubjectId",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                $(".loading").show();
            },
            complete: function () {
                $(".loading").hide();
            },
            success: function (result) {
                $("#topicId").html(result.response);
                $("#topicId").prop("disabled", false); //makes select enabled
                //  $('.selectpicker').selectpicker('refresh');

                console.log(result);
            },
        });
    });

    $("#topicId").on("change", function () {
        var topicId = this.value;
        //$("#noOfSessionn").prop("disabled", false);
        $.ajax({
            type: "POST",
            data: {
                topicId: topicId,
            },
            url: "/frontend/topic/getPlanByTopicId",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                $(".loading").show();
            },
            complete: function () {
                $(".loading").hide();
            },
            success: function (result) {
                $("#noOfSessionn").html(result.response);
                $("#noOfSessionn").prop("disabled", false); //makes select enabled
                //  $('.selectpicker').selectpicker('refresh');

                console.log(result);
            },
        });
    });

    $("#noOfSessionn").on("change", function () {
        var qty = this.value;
        var topicId = $("#topicId").val();

        $.ajax({
            type: "POST",
            data: {
                topicId: topicId,
                qty: qty,
            },
            url: "/frontend/topic/getPlanByTopicQty",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            beforeSend: function () {
                $(".loading").show();
            },
            complete: function () {
                $(".loading").hide();
            },
            success: function (result) {
               // console.log(result.response);

                  $('#amount').val(result.response.amount);
                //   $('#topicId').prop('disabled', false); //makes select enabled
                //  $('.selectpicker').selectpicker('refresh');

                console.log(result);
            },
        });
    });
});
